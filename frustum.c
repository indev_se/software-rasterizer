#include "frustum.h"
#include "triangle.h"
#include "vec_n.h"
#include "shaderprogram.h"
#include <stdio.h>

#define FRUSTUM_PLANE_NEAR        0
#define FRUSTUM_PLANE_FAR         1
#define FRUSTUM_PLANE_LEFT        2
#define FRUSTUM_PLANE_RIGHT       3
#define FRUSTUM_PLANE_TOP         4
#define FRUSTUM_PLANE_BOTTOM      5

void frustum_from_mat(mat4x4 mat, plane frustum_planes[6]) {
    /*
    float m11 = mat[0][0]; // 12
    float m12 = mat[0][1];
    float m13 = mat[0][2];
    float m14 = mat[0][3];
    float m21 = mat[1][0]; // 21
    float m22 = mat[1][1];
    float m23 = mat[1][2];
    float m24 = mat[1][3];
    float m31 = mat[2][0]; // 31
    float m32 = mat[2][1];
    float m33 = mat[2][2];
    float m34 = mat[2][3];
    float m41 = mat[3][0]; // 41
    float m42 = mat[3][1];
    float m43 = mat[3][2];
    float m44 = mat[3][3];
     */
    float m11 = mat[0][0]; // 12
    float m12 = mat[1][0];
    float m13 = mat[2][0];
    float m14 = mat[3][0];

    float m21 = mat[0][1]; // 21
    float m22 = mat[1][1];
    float m23 = mat[2][1];
    float m24 = mat[3][1];

    float m31 = mat[0][2]; // 31
    float m32 = mat[1][2];
    float m33 = mat[2][2];
    float m34 = mat[3][2];

    float m41 = mat[0][3]; // 41
    float m42 = mat[1][3];
    float m43 = mat[2][3];
    float m44 = mat[3][3];

    plane_set_coefficients(m31 + m41, m32 + m42, m33 + m43, m34 + m44, frustum_planes[FRUSTUM_PLANE_NEAR]);
    plane_set_coefficients(-m31 + m41, -m32 + m42, -m33 + m43, -m34 + m44, frustum_planes[FRUSTUM_PLANE_FAR]);

    plane_set_coefficients(m11 + m41, m12 + m42, m13 + m43, m14 + m44, frustum_planes[FRUSTUM_PLANE_LEFT]);
    plane_set_coefficients(-m11 + m41, -m12 + m42, -m13 + m43, -m14 + m44, frustum_planes[FRUSTUM_PLANE_RIGHT]);

    plane_set_coefficients(-m21 + m41, -m22 + m42, -m23 + m43, -m24 + m44, frustum_planes[FRUSTUM_PLANE_TOP]);
    plane_set_coefficients(m21 + m41, m22 + m42, m23 + m43, m24 + m44, frustum_planes[FRUSTUM_PLANE_BOTTOM]);
}

float frustum_place_clip_line(uint8_t is_neg_plane, float comp0, float w0, float comp1, float w1) {

    float denominator;
    float segment;

    if (is_neg_plane)
        denominator = w0 - w1 + comp0 - comp1;
    else
        denominator = w0 - w1 - comp0 + comp1;

    if (denominator == 0)
        return -1;

    if (is_neg_plane)
        segment = (w0 + comp0) / denominator;
    else
        segment = (w0 - comp0) / denominator;

    if (segment >= 0.0f && segment <= 1.0f)
        return segment; // intersect
    else
        return -1.0f; // line segment not intersect
}

void frustum_plane_clip_edge(float line_segment, vs_out_t vertex0, vs_out_t vertex1, vs_out_t *out) {
    uint32_t i;
    size_t vec_size;

    vec4_lerp(out->result, line_segment, vertex0.result, vertex1.result);

    for (i = 0; i < out->varying->length; ++i) {
        vec_size = out->varying->properties[i].length;
        vec_n_lerp(vec_size, out->varying->properties[i].values, line_segment, vertex0.varying->properties[i].values, vertex1.varying->properties[i].values);
    }
}

void frustum_plane_clip_triangle(
        uint8_t is_neg_plane,
        vs_out_t in_vertices[3],
        uint8_t is_outside[3],
        float comp[3],
        float w[3],
        vs_out_t *out_vertices,
        uint32_t *num_triangles) {

    float line0_segment, line1_segment, line2_segment;
    
    if (is_outside[0] == 1 && is_outside[1] == 1 && is_outside[2] == 1) {
        *num_triangles = 0;
        return;
    } else if (is_outside[0] == 0 && is_outside[1] == 1 && is_outside[2] == 1) {

        line0_segment = frustum_place_clip_line(is_neg_plane, comp[1], w[1], comp[0], w[0]);
        line2_segment = frustum_place_clip_line(is_neg_plane, comp[2], w[2], comp[0], w[0]);

        frustum_plane_clip_edge(0, in_vertices[0], in_vertices[0], out_vertices++);
        frustum_plane_clip_edge(line0_segment, in_vertices[1], in_vertices[0], out_vertices++);
        frustum_plane_clip_edge(line2_segment, in_vertices[2], in_vertices[0], out_vertices++);

        *num_triangles = 1;
        return;
    } else if (is_outside[0] == 1 && is_outside[1] == 0 && is_outside[2] == 1) {
        line0_segment = frustum_place_clip_line(is_neg_plane, comp[0], w[0], comp[1], w[1]);
        line1_segment = frustum_place_clip_line(is_neg_plane, comp[2], w[2], comp[1], w[1]);

        frustum_plane_clip_edge(0, in_vertices[1], in_vertices[1], out_vertices++);
        frustum_plane_clip_edge(line0_segment, in_vertices[2], in_vertices[1], out_vertices++);
        frustum_plane_clip_edge(line1_segment, in_vertices[0], in_vertices[1], out_vertices++);

        *num_triangles = 1;
        return;
    } else if (is_outside[0] == 1 && is_outside[1] == 1 && is_outside[2] == 0) {
        line1_segment = frustum_place_clip_line(is_neg_plane, comp[1], w[1], comp[2], w[2]);
        line2_segment = frustum_place_clip_line(is_neg_plane, comp[0], w[0], comp[2], w[2]);

        frustum_plane_clip_edge(0, in_vertices[2], in_vertices[2], out_vertices++);
        frustum_plane_clip_edge(line2_segment, in_vertices[0], in_vertices[2], out_vertices++);
        frustum_plane_clip_edge(line1_segment, in_vertices[1], in_vertices[2], out_vertices++);

        *num_triangles = 1;
        return;
    } else if (is_outside[0] == 0 && is_outside[1] == 0 && is_outside[2] == 1) {

        line1_segment = frustum_place_clip_line(is_neg_plane, comp[2], w[2], comp[1], w[1]);
        line2_segment = frustum_place_clip_line(is_neg_plane, comp[2], w[2], comp[0], w[0]);

        // tri 1
        frustum_plane_clip_edge(0, in_vertices[0], in_vertices[0], out_vertices++);
        frustum_plane_clip_edge(0, in_vertices[1], in_vertices[1], out_vertices++);
        frustum_plane_clip_edge(line1_segment, in_vertices[2], in_vertices[1], out_vertices++);

        // tri 2
        frustum_plane_clip_edge(0, in_vertices[0], in_vertices[0], out_vertices++);
        frustum_plane_clip_edge(line1_segment, in_vertices[2], in_vertices[1], out_vertices++);
        frustum_plane_clip_edge(line2_segment, in_vertices[2], in_vertices[0], out_vertices++);

        *num_triangles = 2;
        return;
    } else if (is_outside[0] == 0 && is_outside[1] == 1 && is_outside[2] == 0) {

        line0_segment = frustum_place_clip_line(is_neg_plane, comp[1], w[1], comp[0], w[0]);
        line1_segment = frustum_place_clip_line(is_neg_plane, comp[1], w[1], comp[2], w[2]);

        // tri 1
        frustum_plane_clip_edge(0, in_vertices[2], in_vertices[2], out_vertices++);
        frustum_plane_clip_edge(0, in_vertices[0], in_vertices[0], out_vertices++);
        frustum_plane_clip_edge(line0_segment, in_vertices[1], in_vertices[0], out_vertices++);

        // tri 2
        frustum_plane_clip_edge(0, in_vertices[2], in_vertices[2], out_vertices++);
        frustum_plane_clip_edge(line0_segment, in_vertices[1], in_vertices[0], out_vertices++);
        frustum_plane_clip_edge(line1_segment, in_vertices[1], in_vertices[2], out_vertices++);
        // some weird here 
        *num_triangles = 2;
        return;
    } else if (is_outside[0] == 1 && is_outside[1] == 0 && is_outside[2] == 0) {
        line0_segment = frustum_place_clip_line(is_neg_plane, comp[0], w[0], comp[1], w[1]);
        line2_segment = frustum_place_clip_line(is_neg_plane, comp[0], w[0], comp[2], w[2]);

        // tri 1
        frustum_plane_clip_edge(0, in_vertices[1], in_vertices[1], out_vertices++);
        frustum_plane_clip_edge(0, in_vertices[2], in_vertices[2], out_vertices++);
        frustum_plane_clip_edge(line2_segment, in_vertices[0], in_vertices[2], out_vertices++);

        // tri 2
        frustum_plane_clip_edge(0, in_vertices[1], in_vertices[1], out_vertices++);
        frustum_plane_clip_edge(line2_segment, in_vertices[0], in_vertices[2], out_vertices++);
        frustum_plane_clip_edge(line0_segment, in_vertices[0], in_vertices[1], out_vertices++);

        *num_triangles = 2;
        return;
    }
    else {
        frustum_plane_clip_edge(0, in_vertices[0], in_vertices[0], out_vertices++);
        frustum_plane_clip_edge(0, in_vertices[1], in_vertices[1], out_vertices++);
        frustum_plane_clip_edge(0, in_vertices[2], in_vertices[2], out_vertices++);

        *num_triangles = 1;
    }
}

void frustum_clip_triangle(plane frustum_planes[6], vs_out_t in_vertices[3], vs_out_t out_vertices[12], vs_out_t tmp_vertices[12], uint32_t *num_triangles) {
    uint32_t i, j;
    float comp[3];
    float w[3];
    uint8_t is_outside[3];
    uint8_t is_neg_plane;
    uint32_t clipped_tris, tris;
    vs_out_t *out_vertices_ptr, *tmp_vertices_ptr;
    vec4 pos[3];
    uint8_t init_tri_num;

    //this.clippingFunc= [this.clipLine_pos, this.clipLine_neg, this.clipLine_pos, this.clipLine_neg, this.clipLine_pos, this.clipLine_neg];
    //this.getComponentFunc= [this.getX, this.getX, this.getY, this.getY, this.getZ, this.getZ];
    //this.isOutsideFunc= [this.isOutside_pos, this.isOutside_neg, this.isOutside_pos, this.isOutside_neg, this.isOutside_pos, this.isOutside_neg];
    // FRUSTUM_PLANE_RIGHT  FRUSTUM_PLANE_LEFT  FRUSTUM_PLANE_BOTTOM    FRUSTUM_PLANE_TOP    FRUSTUM_PLANE_NEAR  FRUSTUM_PLANE_FAR

    out_vertices[0] = in_vertices[0];
    out_vertices[1] = in_vertices[1];
    out_vertices[2] = in_vertices[2];

    *num_triangles = 1;
    init_tri_num = 0;
    out_vertices_ptr = out_vertices;
    tmp_vertices_ptr = tmp_vertices;
    clipped_tris = 0;

    for (i = 0; i < 6; ++i) {
        for (j = 0; j < (*num_triangles)*3; j += 3) {
            vec4_dup(pos[0], out_vertices[j].result);
            vec4_dup(pos[1], out_vertices[j + 1].result);
            vec4_dup(pos[2], out_vertices[j + 2].result);

            in_vertices[0] = out_vertices[j];
            in_vertices[1] = out_vertices[j + 1];
            in_vertices[2] = out_vertices[j + 2];

            is_neg_plane = (i == FRUSTUM_PLANE_LEFT || i == FRUSTUM_PLANE_TOP || i == FRUSTUM_PLANE_FAR);

            if (i == FRUSTUM_PLANE_NEAR || i == FRUSTUM_PLANE_FAR) {
                comp[0] = pos[0][2];
                comp[1] = pos[1][2];
                comp[2] = pos[2][2];

                if (i == FRUSTUM_PLANE_NEAR) {
                    is_outside[0] = comp[0] > pos[0][3] ? 1 : 0;
                    is_outside[1] = comp[1] > pos[1][3] ? 1 : 0;
                    is_outside[2] = comp[2] > pos[2][3] ? 1 : 0;
                } else if (i == FRUSTUM_PLANE_FAR) {
                    is_outside[0] = comp[0] < -pos[0][3] ? 1 : 0;
                    is_outside[1] = comp[1] < -pos[1][3] ? 1 : 0;
                    is_outside[2] = comp[2] < -pos[2][3] ? 1 : 0;
                }
            } else if (i == FRUSTUM_PLANE_LEFT || i == FRUSTUM_PLANE_RIGHT) {
                comp[0] = pos[0][0];
                comp[1] = pos[1][0];
                comp[2] = pos[2][0];

                if (i == FRUSTUM_PLANE_RIGHT) {
                    is_outside[0] = comp[0] > pos[0][3] ? 1 : 0;
                    is_outside[1] = comp[1] > pos[1][3] ? 1 : 0;
                    is_outside[2] = comp[2] > pos[2][3] ? 1 : 0;
                } else if (i == FRUSTUM_PLANE_LEFT) {
                    is_outside[0] = comp[0] < -pos[0][3] ? 1 : 0;
                    is_outside[1] = comp[1] < -pos[1][3] ? 1 : 0;
                    is_outside[2] = comp[2] < -pos[2][3] ? 1 : 0;
                }
            } else if (i == FRUSTUM_PLANE_TOP || i == FRUSTUM_PLANE_BOTTOM) {
                comp[0] = pos[0][1];
                comp[1] = pos[1][1];
                comp[2] = pos[2][1];

                if (i == FRUSTUM_PLANE_BOTTOM) {
                    is_outside[0] = comp[0] > pos[0][3] ? 1 : 0;
                    is_outside[1] = comp[1] > pos[1][3] ? 1 : 0;
                    is_outside[2] = comp[2] > pos[2][3] ? 1 : 0;
                } else if (i == FRUSTUM_PLANE_TOP) {
                    is_outside[0] = comp[0] < -pos[0][3] ? 1 : 0;
                    is_outside[1] = comp[1] < -pos[1][3] ? 1 : 0;
                    is_outside[2] = comp[2] < -pos[2][3] ? 1 : 0;
                }
            }

            w[0] = pos[0][3];
            w[1] = pos[1][3];
            w[2] = pos[2][3];

            frustum_plane_clip_triangle(
                    is_neg_plane,
                    in_vertices,
                    is_outside,
                    comp,
                    w,
                    tmp_vertices_ptr,
                    &tris);

            clipped_tris += tris;
            tmp_vertices_ptr += tris * 3;
        }

        tmp_vertices_ptr = tmp_vertices;
        
        if (clipped_tris > init_tri_num) {
            *num_triangles = clipped_tris;
            init_tri_num = *num_triangles;
        }
        else if (clipped_tris == 0) {
            *num_triangles = 0;
        }
         
        // just to verify that I'm breaking everything because of a override
        // this is slow and should be handled differently
        for (j = 0; j < (*num_triangles)*3; j += 3) {
            shader_vs_out_copy(&out_vertices[j], &tmp_vertices[j]);
            shader_vs_out_copy(&out_vertices[j+1], &tmp_vertices[j+1]);
            shader_vs_out_copy(&out_vertices[j+2], &tmp_vertices[j+2]);
        }
        
        clipped_tris = 0;
    }
}

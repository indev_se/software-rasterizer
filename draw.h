#ifndef DRAW_H_
#define DRAW_H_

#include "framebuffer.h"
#include "vec2.h"

void draw_line( framebuffer_t *pixels,  uint32_t color, vec2 from, vec2 to );

#endif

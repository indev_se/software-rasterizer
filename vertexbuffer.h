#ifndef VERTEXBUFFER_H
#define VERTEXBUFFER_H

#include "vertex.h"

void vertexbuffer_allocate( uint8_t **out, uint32_t num_vertices, vertex_description_t *description );

void vb_read_float32( uint8_t *vb, uint32_t position, float *out );
void vb_read3_float32( uint8_t *vb, uint32_t position, float *out0, float *out1, float *out2 );
void vb_read4_float32( uint8_t *vb, uint32_t position, float *out0, float *out1, float *out2, float *out3 );

void vb_write_float32( uint8_t *vb, uint32_t position, float value );
void vb_write3_float32( uint8_t *vb, uint32_t position, float value0, float value1, float value2 );
void vb_write4_float32( uint8_t *vb, uint32_t position, float value0, float value1, float value2, float value3 );

#endif

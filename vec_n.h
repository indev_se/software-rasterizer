#ifndef VEC_N_H_
#define VEC_N_H_

#include "defines.h"
STATIC_INLINE void vec_n_dup(size_t n, float r[], float a[])
{
	int i;
	for(i=0; i<n; ++i)
		r[i] = a[i];
}

STATIC_INLINE void vec_n_add(size_t n, float r[], float a[], float b[])
{
	int i;
	for(i=0; i<n; ++i)
		r[i] = a[i] + b[i];
}

STATIC_INLINE void vec_n_sub(size_t n, float r[], float a[], float b[]) {
 	int i;
	for(i=0; i<n; ++i)
		r[i] = a[i] - b[i];   
}

STATIC_INLINE void vec_n_scale(size_t n, float r[], float v[], float s)
{
	int i;
	for(i=0; i<n; ++i)
		r[i] = v[i] * s;
}
STATIC_INLINE void vec_n_lerp(size_t n, float r[], float v, float a[], float b[])
{
    int i;
    
    vec_n_sub(n, r, b, a);
    for(i=0; i<n; ++i)
        r[i] = a[i] + r[i] * v;
}

#endif /* VEC_N_H_ */

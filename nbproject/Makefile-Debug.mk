#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=MinGW-Windows
CND_DLIB_EXT=dll
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/context.o \
	${OBJECTDIR}/defaultshaders.o \
	${OBJECTDIR}/draw.o \
	${OBJECTDIR}/framebuffer.o \
	${OBJECTDIR}/frustum.o \
	${OBJECTDIR}/inras.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/rasterizer.o \
	${OBJECTDIR}/renderjob.o \
	${OBJECTDIR}/renderqueue.o \
	${OBJECTDIR}/shaderlang.o \
	${OBJECTDIR}/shaderprogram.o \
	${OBJECTDIR}/system.o \
	${OBJECTDIR}/triangle.o \
	${OBJECTDIR}/types.o \
	${OBJECTDIR}/vertex.o \
	${OBJECTDIR}/vertexbuffer.o \
	${OBJECTDIR}/vertextransform.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-Llibs/windows/SDL2-2.0.0-mingw/lib

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/software-rasterizer.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/software-rasterizer.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/software-rasterizer ${OBJECTFILES} ${LDLIBSOPTIONS} -lmingw32 -lSDL2main -lSDL2

${OBJECTDIR}/context.o: context.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -Ilibs/general/uthash-1.9.8/src -Ilibs/windows/SDL2-2.0.0-mingw/include -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/context.o context.c

${OBJECTDIR}/defaultshaders.o: defaultshaders.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -Ilibs/general/uthash-1.9.8/src -Ilibs/windows/SDL2-2.0.0-mingw/include -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/defaultshaders.o defaultshaders.c

${OBJECTDIR}/draw.o: draw.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -Ilibs/general/uthash-1.9.8/src -Ilibs/windows/SDL2-2.0.0-mingw/include -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/draw.o draw.c

${OBJECTDIR}/framebuffer.o: framebuffer.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -Ilibs/general/uthash-1.9.8/src -Ilibs/windows/SDL2-2.0.0-mingw/include -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/framebuffer.o framebuffer.c

${OBJECTDIR}/frustum.o: frustum.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -Ilibs/general/uthash-1.9.8/src -Ilibs/windows/SDL2-2.0.0-mingw/include -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/frustum.o frustum.c

${OBJECTDIR}/inras.o: inras.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -Ilibs/general/uthash-1.9.8/src -Ilibs/windows/SDL2-2.0.0-mingw/include -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/inras.o inras.c

${OBJECTDIR}/main.o: main.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -Ilibs/general/uthash-1.9.8/src -Ilibs/windows/SDL2-2.0.0-mingw/include -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.c

${OBJECTDIR}/rasterizer.o: rasterizer.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -Ilibs/general/uthash-1.9.8/src -Ilibs/windows/SDL2-2.0.0-mingw/include -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/rasterizer.o rasterizer.c

${OBJECTDIR}/renderjob.o: renderjob.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -Ilibs/general/uthash-1.9.8/src -Ilibs/windows/SDL2-2.0.0-mingw/include -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/renderjob.o renderjob.c

${OBJECTDIR}/renderqueue.o: renderqueue.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -Ilibs/general/uthash-1.9.8/src -Ilibs/windows/SDL2-2.0.0-mingw/include -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/renderqueue.o renderqueue.c

${OBJECTDIR}/shaderlang.o: shaderlang.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -Ilibs/general/uthash-1.9.8/src -Ilibs/windows/SDL2-2.0.0-mingw/include -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/shaderlang.o shaderlang.c

${OBJECTDIR}/shaderprogram.o: shaderprogram.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -Ilibs/general/uthash-1.9.8/src -Ilibs/windows/SDL2-2.0.0-mingw/include -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/shaderprogram.o shaderprogram.c

${OBJECTDIR}/system.o: system.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -Ilibs/general/uthash-1.9.8/src -Ilibs/windows/SDL2-2.0.0-mingw/include -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/system.o system.c

${OBJECTDIR}/triangle.o: triangle.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -Ilibs/general/uthash-1.9.8/src -Ilibs/windows/SDL2-2.0.0-mingw/include -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/triangle.o triangle.c

${OBJECTDIR}/types.o: types.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -Ilibs/general/uthash-1.9.8/src -Ilibs/windows/SDL2-2.0.0-mingw/include -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/types.o types.c

${OBJECTDIR}/vertex.o: vertex.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -Ilibs/general/uthash-1.9.8/src -Ilibs/windows/SDL2-2.0.0-mingw/include -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/vertex.o vertex.c

${OBJECTDIR}/vertexbuffer.o: vertexbuffer.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -Ilibs/general/uthash-1.9.8/src -Ilibs/windows/SDL2-2.0.0-mingw/include -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/vertexbuffer.o vertexbuffer.c

${OBJECTDIR}/vertextransform.o: vertextransform.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -g -Ilibs/general/uthash-1.9.8/src -Ilibs/windows/SDL2-2.0.0-mingw/include -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/vertextransform.o vertextransform.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/software-rasterizer.exe

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc

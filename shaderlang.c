#include "defines.h"
#include "shaderlang.h"

#include "uthash.h"

#include <stdlib.h>

typedef struct {

	uint32_t id;

	float *values;
	uint8_t length;

	UT_hash_handle hh;         /* makes this structure hashable */
} shader_lang_global_variable;

shader_lang_global_variable *global_vars = NULL;

void shlang_register_global_mat4x4( uint32_t id, mat4x4 values )
{
	// first copy everything to a mem aligned float array (one dimensional)
	float *arr;
	uint8_t j, i, index;
	index = 0;

	arr = malloc(sizeof(float)*16);
	for(j=0; j<4; ++j) {
		for(i=0; i<4; ++i) {
			arr[index++] = values[i][j];
		}
	}

	// save it the hash
	shlang_register_global(id, arr, 16);

	//clean up
	free(arr);
}

void shlang_register_global_vec4( uint32_t id, vec4 values )
{
	shlang_register_global(id, values, 4);
}

void shlang_register_global( uint32_t id, float *values, uint8_t length )
{
	shader_lang_global_variable *var;

	HASH_FIND_INT(global_vars, &id, var);

	if ( var == NULL ) {
		var = malloc(sizeof(shader_lang_global_variable));
		var->id = id;
		var->values = malloc(sizeof(float)*length);
		var->length = length;

		HASH_ADD_INT( global_vars, id, var );
	}
	memcpy(var->values, values, sizeof(float)*length);
}

int8_t shlang_get_global_length( uint32_t id )
{
	shader_lang_global_variable *var;

	HASH_FIND_INT(global_vars, &id, var);

	if ( var == NULL )
		return -1;

	return var->length;
}


void shlang_get_global( uint32_t id, float **values )
{
	shader_lang_global_variable *var;

	HASH_FIND_INT(global_vars, &id, var);

	if ( var == NULL )
		*values = NULL;
	else
		*values = var->values;
}

void shlang_get_global_mat4x4( uint32_t id, mat4x4 values )
{
	// first read everything to a mem aligned float array (one dimensional)
	float *arr;
	uint8_t j, i, index;
	index = 0;

	shlang_get_global(id, &arr);

	for(j=0; j<4; ++j) {
		for(i=0; i<4; ++i) {
			values[i][j] = arr[index++];
		}
	}

}

void shlang_get_global_vec4( uint32_t id, vec4 values )
{
	shlang_get_global(id, &values);
}

#ifndef DEFAULTSHADERS_H_
#define DEFAULTSHADERS_H_

#include "shaderprogram.h"

void create_vertex_color_shader( shader_program_t **program );
void create_vertex_white_shader( shader_program_t **program );

#endif /* DEFAULTSHADERS_H_ */

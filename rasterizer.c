#include "framebuffer.h"
#include "context.h"
#include "math.h"
#include "vec2.h"
#include "vec_n.h"
#include "draw.h"
#include "rasterizer.h"
#include "shaderprogram.h"
#include "shaderstructs.h"

#include <stdlib.h>

void rasterize_triangle( vs_out_t *v0, vs_out_t *v1, vs_out_t *v2, shader_program_t *program )
{
	uint32_t i;

	vertex_shader_t *vs;
	pixel_shader_t *ps;

	vs = program->vertex_shader;
	ps = program->pixel_shader;

	ps_out_t *ps_out = malloc(sizeof(ps_out_t));

	context_t *context = get_current_context();

	framebuffer_t *colorbuffer = context->framebuffer;
	framebuffer_t *depthbuffer = context->depthbuffer;

	uint32_t width = context->framebuffer->width;
	uint32_t height = context->framebuffer->height;

	vec2 p0, p1, p2;

	p0[0] = width * ((v0->result[0] + 1.0)/2.0);
	p0[1] = height * ((v0->result[1] + 1.0) / 2.0);

	p1[0] = width * ((v1->result[0] + 1.0)/2.0);
	p1[1] = height * ((v1->result[1] + 1.0) / 2.0);

	p2[0] = width * ((v2->result[0] + 1.0)/2.0);
	p2[1] = height * ((v2->result[1] + 1.0) / 2.0);

	if (p0[0] < 0) p0[0] = 0;
	if (p0[1] < 0) p0[1] = 0;
	if (p1[0] < 0) p1[0] = 0;
	if (p1[1] < 0) p1[1] = 0;
	if (p2[0] < 0) p2[0] = 0;
	if (p2[1] < 0) p2[1] = 0;

	float minx = MIN(MIN(p0[0], p1[0]), p2[0]);
	float maxx = MAX(MAX(p0[0], p1[0]), p2[0]);
	float miny = MIN(MIN(p0[1], p1[1]), p2[1]);
	float maxy = MAX(MAX(p0[1], p1[1]), p2[1]);

	float dx01 = p0[0] - p1[0];
	float dx12 = p1[0] - p2[0];
	float dx20 = p2[0] - p0[0];

	float dy01 = p0[1] - p1[1];
	float dy12 = p1[1] - p2[1];
	float dy20 = p2[1] - p0[1];

	float denom = (dx12 * dy20 - dy12 * dx20);

	for ( uint32_t x = minx; x <= maxx; ++x )
	{
		float cx0 = dy01 * (x - p0[0]);
		float cx1 = dy12 * (x - p1[0]);
		float cx2 = dy20 * (x - p2[0]);

		for ( uint32_t y = miny; y <= maxy; ++y )
		{
			float t1 = (cx0) - (dx01) * (y - p0[1]);
			float t2 = (cx1) - (dx12) * (y - p1[1]);
			float t3 = (cx2) - (dx20) * (y - p2[1]);

			if ( (t1 > 0 && t2 > 0 && t3 > 0) || (t1 < 0 && t2 < 0 && t3 < 0) )
			{
				// calculate the barycentrix coordinates
				float b0 = t2 / denom;
				float b1 = t3 / denom;
				float b2 = 1.0f - b0 - b1;

				// calculate the new z
				float z = (v0->result[2] * b0) + (v1->result[2] * b1) + (v2->result[2] * b2);
				float currentZ = framebuffer_getpixel16float(depthbuffer, x, y);

				// if the new z is less than exising then run the pixel shader and write the value to the out buffer
				if ( z < currentZ ) {
					uint32_t rgb;

					// calculate the varying for this pixel
					vec4 varying0, varying1, varying2;
					size_t vec_size;
					for ( i = 0; i < vs->out->varying->length; ++i ) {
						vec_size = v0->varying->properties[i].length;

						// scale the varying
						vec_n_scale(vec_size, varying0, v0->varying->properties[i].values, b0);
						vec_n_scale(vec_size, varying1, v1->varying->properties[i].values, b1);
						vec_n_scale(vec_size, varying2, v2->varying->properties[i].values, b2);

						// add them up
						vec_n_add(vec_size, ps->in->varying->properties[i].values, varying0, varying1);
						vec_n_add(vec_size, ps->in->varying->properties[i].values, ps->in->varying->properties[i].values, varying2);
					}

					// run the pixelshader
					ps->func( ps->in, ps_out);

					// convert the values to a color;
					rgb = (uint32_t)(0xff * ps_out->result[0]) << 16 | (uint32_t)(0xff * ps_out->result[1]) << 8 | (uint32_t)(0xff * ps_out->result[2]);

					// write the pixel color value
					framebuffer_putpixel24int(colorbuffer, rgb, x, y);

					// write the new z value
					framebuffer_putpixel16float(depthbuffer, z, x, y);
				}
			}
		}
	}

	// clean up
	free(ps_out);
}

void rasterize_triangle_line(vs_out_t *v0, vs_out_t *v1, vs_out_t *v2, shader_program_t *program)
{
    context_t *context = get_current_context();

    framebuffer_t *colorbuffer = context->framebuffer;

    uint32_t width = context->framebuffer->width - 1;
    uint32_t height = context->framebuffer->height;

    vec2 p0, p1, p2;

    p0[0] = width * ((v0->result[0] + 1.0)/2.0);
    p0[1] = height * ((v0->result[1] + 1.0) / 2.0);

    p1[0] = width * ((v1->result[0] + 1.0)/2.0);
    p1[1] = height * ((v1->result[1] + 1.0) / 2.0);

    p2[0] = width * ((v2->result[0] + 1.0)/2.0);
    p2[1] = height * ((v2->result[1] + 1.0) / 2.0);

    if (p0[0] < 0) p0[0] = 0;
    if (p0[1] < 0) p0[1] = 0;
    if (p1[0] < 0) p1[0] = 0;
    if (p1[1] < 0) p1[1] = 0;
    if (p2[0] < 0) p2[0] = 0;
    if (p2[1] < 0) p2[1] = 0;

    draw_line( colorbuffer, 0xffffff, p0, p1 );
    draw_line( colorbuffer, 0xffffff, p1, p2 );
    draw_line( colorbuffer, 0xffffff, p2, p0 );
}

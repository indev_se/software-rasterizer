#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include "vertex.h"
#include "vertexbuffer.h"

void vertexbuffer_allocate( uint8_t **out, uint32_t num_vertices, vertex_description_t *description )
{
	uint8_t *vb = malloc( num_vertices * description->vertex_size );
	
	*out = vb;
}

inline void vb_read_float32( uint8_t *vb, uint32_t position, float *out )
{
	*out = *( (float*)(vb + position) );
}

inline void vb_read3_float32( uint8_t *vb, uint32_t position, float *out0, float *out1, float *out2 )
{
	*out0 = *( (float*)(vb + position) );
	*out1 = *( (float*)(vb + position + 4) );
	*out2 = *( (float*)(vb + position + 8) );
}

inline void vb_read4_float32( uint8_t *vb, uint32_t position, float *out0, float *out1, float *out2, float *out3 )
{
	*out0 = *( (float*)(vb + position) );
	*out1 = *( (float*)(vb + position + 4) );
	*out2 = *( (float*)(vb + position + 8) );
	*out3 = *( (float*)(vb + position + 12) );
}

inline void vb_write_float32( uint8_t *vb, uint32_t position, float value )
{
	*( (float*)(vb + position) ) = value;
}

inline void vb_write3_float32( uint8_t *vb, uint32_t position, float value0, float value1, float value2 )
{
	*( (float*)(vb + position) ) = value0;
	*( (float*)(vb + position + 4) ) = value1;
	*( (float*)(vb + position + 8) ) = value2;
}

inline void vb_write4_float32( uint8_t *vb, uint32_t position, float value0, float value1, float value2, float value3 )
{
	*( (float*)(vb + position) ) = value0;
	*( (float*)(vb + position + 4) ) = value1;
	*( (float*)(vb + position + 8) ) = value2;
	*( (float*)(vb + position + 12) ) = value3;
}

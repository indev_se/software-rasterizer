#ifndef VERTEX_H
#define VERTEX_H

#include "defines.h"
#include "types.h"
#include "vec3.h"

typedef struct {
	uint32_t size; // size in bytes of the vertex element
	uint32_t location; // locaton that will be used as identifier in the shader
	uint32_t offset; // offset/stride in bytes
	uint32_t num_elements; // number of elements of type
	uint32_t type; // data type
} vertex_attribute_t;

typedef struct {
	vertex_attribute_t *attributes;
	uint8_t num_attributes;
	uint32_t vertex_format; // vertex format (xyz, color_rgb etc)
	
	uint32_t vertex_size;
} vertex_description_t;

void vertex_create_description( vertex_description_t **out, uint32_t vertex_format );

void vertex_add_attribute( vertex_description_t *description, uint8_t location, uint32_t type, uint32_t num_elements );

void vertex_lock_description( vertex_description_t *description );

void vertex_attribute_get_by_location( vertex_attribute_t **out, vertex_description_t *description, uint32_t location );


#endif

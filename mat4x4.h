#ifndef MAT4X4_H
#define MAT4X4_H

#include <math.h>
#include <string.h>
#include "math.h"
#include "vec4.h"
#include "vec3.h"
#include "defines.h"

typedef vec4 mat4x4[4];
STATIC_INLINE void mat4x4_identity(mat4x4 M)
{
	int i, j;
	for(j=0; j<4; ++j) for(i=0; i<4; ++i) {
		M[i][j] = i==j ? 1.0f : 0.0f;
	}
}
STATIC_INLINE void mat4x4_dup(mat4x4 M, mat4x4 N)
{
	int i, j;
	for(j=0; j<4; ++j) {
		for(i=0; i<4; ++i) {
			M[i][j] = N[i][j];
		}
	}
}
STATIC_INLINE void mat4x4_add(mat4x4 M, mat4x4 a, mat4x4 b)
{
	int i;
	for(i=0; i<4; ++i)
		vec4_add(M[i], a[i], b[i]);
}
STATIC_INLINE void mat4x4_sub(mat4x4 M, mat4x4 a, mat4x4 b)
{
	int i;
	for(i=0; i<4; ++i)
		vec4_sub(M[i], a[i], b[i]);
}
STATIC_INLINE void mat4x4_scale(mat4x4 M, mat4x4 a, float k)
{
	int i;
	for(i=0; i<4; ++i)
		vec4_scale(M[i], a[i], k);
}
STATIC_INLINE void mat4x4_scale_aniso(mat4x4 M, mat4x4 a, float x, float y, float z)
{
	vec4_scale(M[0], a[0], x);
	vec4_scale(M[1], a[1], y);
	vec4_scale(M[2], a[2], z);
}
STATIC_INLINE void mat4x4_mul(mat4x4 M, mat4x4 a, mat4x4 b)
{
	int k, r, c;
	mat4x4 R;
	for(r=0; r<4; ++r) for(c=0; c<4; ++c) {
		R[c][r] = 0;
		for(k=0; k<4; ++k) {
			R[c][r] += a[k][r] * b[c][k];
		}
	}
	memcpy(M, R, sizeof(R));
}
STATIC_INLINE void mat4x4_mul_vec4(vec4 r, mat4x4 M, vec4 v)
{
	vec4 r_;
	int i, j;
	for(j=0; j<4; ++j) {
		r_[j] = 0.;
		for(i=0; i<4; ++i) {
			r_[j] += M[i][j] * v[i];
		}
	}
	memcpy(r, r_, sizeof(r_));
}
STATIC_INLINE void mat4x4_translate(mat4x4 T, float x, float y, float z)
{
	mat4x4_identity(T);
	T[3][0] = x;
	T[3][1] = y;
	T[3][2] = z;
}
STATIC_INLINE void mat4x4_from_vec3_mul_outer(mat4x4 M, vec3 a, vec3 b)
{
	int i, j;
	for(i=0; i<4; ++i) for(j=0; j<4; ++j) {
		M[i][j] = i<3 && j<3 ? a[i] * b[j] : 0.f;
	}
}
STATIC_INLINE void mat4x4_rotate(mat4x4 R, mat4x4 M, float x, float y, float z, float angle)
{
	mat4x4 S, T, C;

	float s = sinf(angle);
	float c = cosf(angle);
	vec3 u = {x, y, z};
	vec3_norm(u, u);

	mat4x4_from_vec3_mul_outer(T, u, u);

	S[0][0] = 0; S[0][1] = u[2]; S[0][2] = -u[1]; S[0][3] = 0;
	S[1][0] = -u[2]; S[1][1] = 0; S[1][2] = u[0]; S[1][3] = 0;
	S[2][0] = u[1]; S[2][1] = -u[0]; S[2][2] = 0; S[2][3] = 0;
	S[2][0] = 0; S[2][1] = 0; S[2][2] = 0; S[2][3] = 0;

	mat4x4_scale(S, S, s);

	mat4x4_identity(C);
	mat4x4_sub(C, C, T);

	mat4x4_scale(C, C, c);

	mat4x4_add(T, T, C);
	mat4x4_add(T, T, S);

	T[3][3] = 1.;		
	mat4x4_mul(R, M, T);
}
STATIC_INLINE void mat4x4_rotate_X(mat4x4 Q, mat4x4 M, float angle)
{
	float s = sinf(angle);
	float c = cosf(angle);
	mat4x4 R = {
		{1, 0, 0, 0},
		{0, c, s, 0},
		{0,-s, c, 0},
		{0, 0, 0, 1}
	};
	mat4x4_mul(Q, M, R);
}
STATIC_INLINE void mat4x4_rotate_Y(mat4x4 Q, mat4x4 M, float angle)
{
	float s = sinf(angle);
	float c = cosf(angle);
	mat4x4 R = {
		{ c, 0, s, 0},
		{ 0, 1, 0, 0},
		{-s, 0, c, 0},
		{ 0, 0, 0, 1}
	};
	mat4x4_mul(Q, M, R);
}
STATIC_INLINE void mat4x4_rotate_Z(mat4x4 Q, mat4x4 M, float angle)
{
	float s = sinf(angle);
	float c = cosf(angle);
	mat4x4 R = {
		{ c, s, 0, 0},
		{-s, c, 0, 0},
		{ 0, 0, 1, 0},
		{ 0, 0, 0, 1}
	};
	mat4x4_mul(Q, M, R);
}
STATIC_INLINE void mat4x4_row(vec4 r, mat4x4 M, int i)
{
	int k;
	for(k=0; k<4; ++k)
		r[k] = M[k][i];
}
STATIC_INLINE void mat4x4_col(vec4 r, mat4x4 M, int i)
{
	int k;
	for(k=0; k<4; ++k)
		r[k] = M[i][k];
}
STATIC_INLINE void mat4x4_transpose(mat4x4 M, mat4x4 N)
{
	int i, j;
	mat4x4 R;
	for(j=0; j<4; ++j) {
		for(i=0; i<4; ++i) {
			R[i][j] = N[j][i];
		}
	}
	memcpy(M, R, sizeof(R));
}
STATIC_INLINE void mat4x4_invert(mat4x4 T, mat4x4 M)
{
	mat4x4 R;
	R[0][0] = M[1][1]*(M[2][2]*M[3][3] - M[2][3]*M[3][2]) - M[2][1]*(M[1][2]*M[3][3] - M[1][3]*M[3][2]) - M[3][1]*(M[1][3]*M[2][2] - M[1][2]*M[2][3]);
	R[0][1] = M[0][1]*(M[2][3]*M[3][2] - M[2][2]*M[3][3]) - M[2][1]*(M[0][3]*M[3][2] - M[0][2]*M[3][3]) - M[3][1]*(M[0][2]*M[2][3] - M[0][3]*M[2][2]);
	R[0][2] = M[0][1]*(M[1][2]*M[3][3] - M[1][3]*M[3][2]) - M[1][1]*(M[0][2]*M[3][3] - M[0][3]*M[3][2]) - M[3][1]*(M[0][3]*M[1][2] - M[0][2]*M[1][3]);
	R[0][3] = M[0][1]*(M[1][3]*M[2][2] - M[1][2]*M[2][3]) - M[1][1]*(M[0][3]*M[2][2] - M[0][2]*M[2][3]) - M[2][1]*(M[0][2]*M[1][3] - M[0][3]*M[1][2]);

	R[1][0] = M[1][0]*(M[2][3]*M[3][2] - M[2][2]*M[3][3]) - M[2][0]*(M[1][3]*M[3][2] - M[1][2]*M[3][3]) - M[3][0]*(M[1][2]*M[2][3] - M[1][3]*M[2][2]);
	R[1][1] = M[0][0]*(M[2][2]*M[3][3] - M[2][3]*M[3][2]) - M[2][0]*(M[0][2]*M[3][3] - M[0][3]*M[3][2]) - M[3][0]*(M[0][3]*M[2][2] - M[0][2]*M[2][3]);
	R[1][2] = M[0][0]*(M[1][3]*M[3][2] - M[1][2]*M[3][3]) - M[1][0]*(M[0][3]*M[3][2] - M[0][2]*M[3][3]) - M[3][0]*(M[0][2]*M[1][3] - M[0][3]*M[1][2]);
	R[1][3] = M[0][0]*(M[1][2]*M[2][3] - M[1][3]*M[2][2]) - M[1][0]*(M[0][2]*M[2][3] - M[0][3]*M[2][2]) - M[2][0]*(M[0][3]*M[1][2] - M[0][2]*M[1][3]);

	R[2][0]  = M[1][0]*(M[2][1]*M[3][3] - M[2][3]*M[3][1]) - M[2][0]*(M[1][1]*M[3][3] - M[1][3]*M[3][1]) - M[3][0]*(M[1][3]*M[2][1] - M[1][1]*M[2][3]);
	R[2][1]  = M[0][0]*(M[2][3]*M[3][1] - M[2][1]*M[3][3]) - M[2][0]*(M[0][3]*M[3][1] - M[0][1]*M[3][3]) - M[3][0]*(M[0][1]*M[2][3] - M[0][3]*M[2][1]);
	R[2][2] = M[0][0]*(M[1][1]*M[3][3] - M[1][3]*M[3][1]) - M[1][0]*(M[0][1]*M[3][3] - M[0][3]*M[3][1]) - M[3][0]*(M[0][3]*M[1][1] - M[0][1]*M[1][3]);
	R[2][3] = M[0][0]*(M[1][3]*M[2][1] - M[1][1]*M[2][3]) - M[1][0]*(M[0][3]*M[2][1] - M[0][1]*M[2][3]) - M[2][0]*(M[0][1]*M[1][3] - M[0][3]*M[1][1]);

	R[3][0] = M[1][0]*(M[2][2]*M[3][1] - M[2][1]*M[3][2]) - M[2][0]*(M[1][2]*M[3][1] - M[1][1]*M[3][2]) - M[3][0]*(M[1][1]*M[2][2] - M[1][2]*M[2][1]);
	R[3][1] = M[0][0]*(M[2][1]*M[3][2] - M[2][2]*M[3][1]) - M[2][0]*(M[0][1]*M[3][2] - M[0][2]*M[3][1]) - M[3][0]*(M[0][2]*M[2][1] - M[0][1]*M[2][2]);
	R[3][2] = M[0][0]*(M[1][2]*M[3][1] - M[1][1]*M[3][2]) - M[1][0]*(M[0][2]*M[3][1] - M[0][1]*M[3][2]) - M[3][0]*(M[0][1]*M[1][2] - M[0][2]*M[1][1]);
	R[3][3] = M[0][0]*(M[1][1]*M[2][2] - M[1][2]*M[2][1]) - M[1][0]*(M[0][1]*M[2][2] - M[0][2]*M[2][1]) - M[2][0]*(M[0][2]*M[1][1] - M[0][1]*M[1][2]);
	memcpy(T, R, sizeof(T));
}
STATIC_INLINE void mat4x4_frustum(mat4x4 M, float l, float r, float b, float t, float n, float f)
{
	M[0][0] = 2.f*n/(r-l);
	M[0][1] = M[0][2] = M[0][3] = 0.f;
	
	M[1][1] = 2.f*n/(t-b);
	M[1][0] = M[1][2] = M[1][3] = 0.f;

	M[2][0] = (r+l)/(r-l);
	M[2][1] = (t+b)/(t-b);
	M[2][2] = -(f+n)/(f-n);
	M[2][3] = -1.f;
	
	M[3][2] = -2.f*(f*n)/(f-n);
	M[3][0] = M[3][1] = M[3][3] = 0.f;
}

STATIC_INLINE void mat4x4_projection(mat4x4 M, float fovy, float aspect, float near, float far)
{
	// http://www.songho.ca/opengl/gl_projectionmatrix.html
	float range = (float)tan((float)DTOR(fovy / 2.0f)) * near;
	float left = -range * aspect;
	float right = range * aspect;
	float bottom = -range;
	float top = range;

	M[0][0] = (2.f * near) / (right - left);
	M[1][1] = (2.f * near) / (top - bottom);
	M[2][2] = - (far + near) / (far - near);
	M[2][3] = - 1.f;
	M[3][2] = - (2.f * far * near) / (far - near);
	M[3][3] = 1;
}

STATIC_INLINE void mat4x4_ortho(mat4x4 M, float l, float r, float b, float t, float n, float f)
{
	M[0][0] = 2.f/(r-l);
	M[0][1] = M[0][2] = M[0][3] = 0.f;

	M[1][1] = 2.f/(t-b);
	M[1][0] = M[1][2] = M[1][3] = 0.f;

	M[2][2] = -2.f/(f-n);
	M[2][0] = M[2][1] = M[2][3] = 0.f;
	
	M[3][0] = -(r+l)/(r-l);
	M[3][1] = -(t+b)/(t-b);
	M[3][2] = -(f+n)/(f-n);
	M[3][3] = 1.f;
}

#endif

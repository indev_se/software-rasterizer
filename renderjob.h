#ifndef RENDERJOB_H
#define RENDERJOB_H

#include <stdint.h>

#include "defines.h"
#include "plane.h"
#include "framebuffer.h"
#include "vertex.h"
#include "mat4x4.h"
#include "shaderprogram.h"

typedef struct {

	uint32_t face_culling;

} renderjob_states_t;

typedef struct {

	mat4x4 mat_modelview;
	mat4x4 mat_projection;

	plane frustum_planes[6];

	uint32_t type; // primitive type
	
	uint8_t *vertices;
	uint32_t num_vertices;
	
	vertex_description_t *vertex_description;

	shader_program_t *program;

	renderjob_states_t *states;

} renderjob_t;


void renderjob_create( renderjob_t **out );
void renderjob_process( renderjob_t *renderjob );

#endif

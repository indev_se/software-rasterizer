#include "vec2.h"
#include "framebuffer.h"
#include "draw.h"

void draw_line( framebuffer_t *pixels, uint32_t color, vec2 from, vec2 to )
{
	uint32_t len;
	vec2 px, pxlen;
	vec2 dir;

	vec2_sub(dir, to, from);
	len = vec2_len( dir );
	vec2_norm(dir, dir);

	for ( uint32_t i = 0; i < len; ++i ) {

		vec2_scale( pxlen, dir, i );

		vec2_add( px, from, pxlen );

		framebuffer_putpixel24int(pixels, color, px[0], px[1]);
	}
}

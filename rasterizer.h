#ifndef RASTERIZER_H
#define RASTERIZER_H

#include "vec3.h"
#include "vertexbuffer.h"
#include "shaderstructs.h"

void rasterize_triangle( vs_out_t *v0, vs_out_t *v1, vs_out_t *v2, shader_program_t *program );

void rasterize_triangle_line( vs_out_t *v0, vs_out_t *v1, vs_out_t *v2, shader_program_t *program );

#endif

#ifndef VEC4_H
#define VEC4_H

#include <math.h>
#include <string.h>

typedef float vec4[4];
STATIC_INLINE void vec4_dup(vec4 r, vec4 a)
{
	int i;
	for(i=0; i<4; ++i)
		r[i] = a[i];
}
STATIC_INLINE void vec4_add(vec4 r, vec4 a, vec4 b)
{
	int i;
	for(i=0; i<4; ++i)
		r[i] = a[i] + b[i];
}
STATIC_INLINE void vec4_sub(vec4 r, vec4 a, vec4 b)
{
	int i;
	for(i=0; i<4; ++i)
		r[i] = a[i] - b[i];
}
STATIC_INLINE void vec4_scale(vec4 r, vec4 v, float s)
{
	int i;
	for(i=0; i<4; ++i)
		r[i] = v[i] * s;
}
STATIC_INLINE float vec4_mul_inner(vec4 a, vec4 b)
{
	float p = 0.;
	int i;
	for(i=0; i<4; ++i)
		p += b[i]*a[i];
	return p;
}
STATIC_INLINE void vec4_mul_cross(vec4 r, vec4 a, vec4 b)
{
	vec4 c;
	c[0] = a[1]*b[2] - a[2]*b[1];
	c[1] = a[2]*b[0] - a[0]*b[2];
	c[2] = a[0]*b[1] - a[1]*b[0];
	c[3] = 1.;
	memcpy(r, c, sizeof(c));
}
STATIC_INLINE float vec4_len(vec4 v)
{
	return sqrtf(vec4_mul_inner(v,v));
}
STATIC_INLINE void vec4_norm(vec4 r, vec4 v)
{
	float k = 1.0f / vec4_len(v);
	vec4_scale(r, v, k);
}
STATIC_INLINE void vec4_lerp(vec4 r, float v, vec4 a, vec4 b)
{
    int i;
    
    vec4_sub(r, b, a);
    for(i=0; i<4; ++i)
        r[i] = a[i] + r[i] * v;
}
#endif

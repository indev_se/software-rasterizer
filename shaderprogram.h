#ifndef SHADERPROGRAM_H_
#define SHADERPROGRAM_H_

#include "defines.h"
#include "vec4.h"
#include "vec_n.h"

#include "shaderstructs.h"

void shader_ps_create( pixel_shader_t **ps );
void shader_ps_destroy( pixel_shader_t *ps );

void shader_vs_create( vertex_shader_t **vs );
void shader_vs_destroy( vertex_shader_t *vs );

uint8_t shader_add_property( shader_properties_t *properties, uint8_t property_length );

void shader_program_compile( vertex_shader_t *vs, pixel_shader_t *ps, shader_program_t **program );

void shader_vs_alloc_out( vertex_shader_t *vs, vs_out_t **out );
void shader_vs_alloc_out_varying( vertex_shader_t *vs, vs_out_t *out );

void shader_vs_out_copy( vs_out_t *dst, vs_out_t *src );

void shader_vs_free_out( vs_out_t *out );
void shader_vs_free_out_varying(vs_out_t *out);

#endif /* SHADERPROGRAM_H_ */

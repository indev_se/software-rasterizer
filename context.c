#include <stdlib.h>

#include "framebuffer.h"
#include "context.h"

context_t *current_context = NULL;

context_t* context_create(void)
{
	context_t *context = (context_t*)malloc(sizeof(context_t));

	mat4x4_identity(context->mat_modelview);
	mat4x4_identity(context->mat_projection);
	
	mat4x4_projection(context->mat_projection, 60, 4/3, 0.1f, 1000.0f);

	context->viewport_width = 800;
	context->viewport_height = 600;

	context->framebuffer = framebuffer_create(context->viewport_width, context->viewport_height, INRAS_RGB24 );
	context->depthbuffer = framebuffer_create(context->viewport_width, context->viewport_height, INRAS_DEPTH16 );

	context->states = malloc(sizeof(context_states_t));
	context->states->front_face = INRAS_FRONTFACE_CW;

	return context;
}

void set_current_context( context_t* context )
{
	current_context = context;
}

context_t* get_current_context(void)
{
	return current_context;
}

/*
 * triangle.c
 *
 *  Created on: 16 nov 2013
 *      Author: Kristian
 */
#include "vec3.h"
#include "plane.h"
#include "shaderstructs.h"
#include "triangle.h"

void triangle_calculate_normal_cw( vec3 norm, vec3 v0, vec3 v1, vec3 v2 )
{
	float rx1 = v1[0] - v0[0];
	float ry1 = v1[1] - v0[1];
	float rz1 = v1[2] - v0[2];

	float rx2 = v2[0] - v0[0];
	float ry2 = v2[1] - v0[1];
	float rz2 = v2[2] - v0[2];

	norm[0] = ry1 * rz2 - ry2 * rz1;
	norm[1] = rz1 * rx2 - rz2 * rx1;
	norm[2] = rx1 * ry2 - rx2 * ry1;

	vec3_norm(norm, norm);
}

void triangle_calculate_normal_ccw( vec3 norm, vec3 v0, vec3 v1, vec3 v2 )
{
	float rx1 = v2[0] - v0[0];
	float ry1 = v2[1] - v0[1];
	float rz1 = v2[2] - v0[2];

	float rx2 = v1[0] - v0[0];
	float ry2 = v1[1] - v0[1];
	float rz2 = v1[2] - v0[2];

	norm[0] = ry1 * rz2 - ry2 * rz1;
	norm[1] = rz1 * rx2 - rz2 * rx1;
	norm[2] = rx1 * ry2 - rx2 * ry1;

	vec3_norm(norm, norm);
}

void triangle_clip_plane( plane clip_plane, vs_out_t in_vertices[3], vs_out_t *out_vertices, uint32_t *num_triangles )
{
	vec4 points[3];
	uint32_t classification;

	vec4_dup(points[0], in_vertices[0].result);
	vec4_dup(points[1], in_vertices[1].result);
	vec4_dup(points[2], in_vertices[2].result);

	classification = plane_classify_points( clip_plane, points, 3 );

	if ( classification == PLANE_POINTS_INSIDE )
	{
		out_vertices = in_vertices;
		*num_triangles = 1;
	}
	else if ( classification == PLANE_POINTS_BOTH )
	{

	}
	else
		*num_triangles = 0;
}

uint8_t triangle_is_valid( vec3 v0, vec3 v1, vec3 v2 )
{
	vec3 norm;

	triangle_calculate_normal_cw(norm, v0, v1, v2);

	float len = vec3_len(norm);

	return (len > .0f ? 1 : 0);
}

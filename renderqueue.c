#include "renderqueue.h"
#include "renderjob.h"
#include "system.h"

#include <stdlib.h>

struct renderjob_queue_node {

	renderjob_t *renderjob;
	
	struct renderjob_queue_node *prev;
	struct renderjob_queue_node *next;
	
};

typedef struct renderjob_queue_node renderjob_queue_node_t;

renderjob_queue_node_t *head = NULL;
renderjob_queue_node_t *curr = NULL;

void renderqueue_destroy(void)
{
	if ( NULL != head )
	{
		renderjob_queue_node_t *ptr, *tmp;
		
		ptr = head;
		while ( ptr->next ) {
			tmp = ptr;
			ptr = ptr->next;
			
			free(tmp);
		}
		free(head);
		
		head = curr = NULL;
	}
}

void renderqueue_create( renderjob_t *renderjob  )
{
	renderjob_queue_node_t *ptr = malloc( sizeof(renderjob_queue_node_t) );
	if ( NULL == ptr )
		errno_exit("Unable to allocate renderjob queue node");
	
	ptr->renderjob = renderjob;
	ptr->next = NULL;
	ptr->prev = NULL;
	
	head = curr = ptr;
}

void renderqueue_add( renderjob_t *renderjob  )
{
	if ( NULL == head ) {
		renderqueue_create(renderjob);
		return;
	}
	
	renderjob_queue_node_t *ptr = malloc( sizeof(renderjob_queue_node_t) );
	
	if ( NULL == ptr )
		errno_exit("Unable to allocate renderjob queue node");
	
	ptr->renderjob = renderjob;
	ptr->next = NULL;
	ptr->prev = curr;

	curr->next = ptr;
	
	curr = ptr;
}

void renderqueue_process(void)
{
	renderjob_queue_node_t *ptr;
	
	ptr = head;
	while ( NULL != ptr )
	{
		renderjob_process( ptr->renderjob );
		
		ptr = ptr->next;
	}
}

/*
 * types.h
 *
 *  Created on: 16 nov 2013
 *      Author: Kristian
 */

#ifndef TYPES_H_
#define TYPES_H_

uint8_t datatype_to_size( uint32_t type );
uint8_t format_to_size( uint32_t type );

#endif /* TYPES_H_ */

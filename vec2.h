#ifndef VEC2_H_
#define VEC2_H_

#include <math.h>
#include <string.h>

#include "defines.h"

typedef float vec2[2];
STATIC_INLINE void vec2_dup(vec2 r, vec2 a)
{
	int i;
	for(i=0; i<2; ++i)
		r[i] = a[i];
}
STATIC_INLINE void vec2_add(vec2 r, vec2 a, vec2 b)
{
	int i;
	for(i=0; i<2; ++i)
		r[i] = a[i] + b[i];
}
STATIC_INLINE void vec2_sub(vec2 r, vec2 a, vec2 b)
{
	int i;
	for(i=0; i<2; ++i)
		r[i] = a[i] - b[i];
}
STATIC_INLINE void vec2_scale(vec2 r, vec2 v, float s)
{
	int i;
	for(i=0; i<2; ++i)
		r[i] = v[i] * s;
}
STATIC_INLINE float vec2_mul_inner(vec2 a, vec2 b)
{
	float p = 0.;
	int i;
	for(i=0; i<2; ++i)
		p += b[i]*a[i];
	return p;
}
STATIC_INLINE float vec2_len(vec2 v)
{
	return sqrtf(vec2_mul_inner(v,v));
}
STATIC_INLINE void vec2_norm(vec2 r, vec2 v)
{
	float k = 1.0f / vec2_len(v);
	vec2_scale(r, v, k);
}
STATIC_INLINE float vec2_dot(vec2 a, vec2 b)
{
	float dot = (a[0] * b[0]) + (a[1] * b[1]);
	return dot;
}

#endif

#ifndef SHADERLANG_H_
#define SHADERLANG_H_

#include "mat4x4.h"
#include "vec4.h"

/* Shader lang constants */
#define SHADER_MODELVIEW_MATRIX		INRAS_SHADERLANG + 1
#define SHADER_PROJECTION_MATRIX	INRAS_SHADERLANG + 2
#define SHADER_MVP_MATRIX			INRAS_SHADERLANG + 3

void shlang_register_global( uint32_t id, float *values, uint8_t length );
void shlang_register_global_mat4x4( uint32_t id, mat4x4 values );
void shlang_register_global_vec4( uint32_t id, vec4 values );

int8_t shlang_get_global_length( uint32_t id );
void shlang_get_global( uint32_t id, float **values );
void shlang_get_global_mat4x4( uint32_t id, mat4x4 values );
void shlang_get_global_vec4( uint32_t id, vec4 values );

#endif /* SHADERLANG_H_ */

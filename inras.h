#ifndef INRAS_H_
#define INRAS_H_

void inras_init(void);
void inras_clear_framebuffers(void);
void inras_flush(void);

#endif /*INRAS_H_*/

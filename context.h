#ifndef CONTEXT_H
#define CONTEXT_H

#include <stdint.h>
#include "mat4x4.h"
#include "framebuffer.h"

typedef struct {

	uint32_t front_face;

} context_states_t;

typedef struct {

	uint32_t viewport_width;
	uint32_t viewport_height;

	mat4x4 mat_projection;
	mat4x4 mat_modelview;

	framebuffer_t *framebuffer;

	framebuffer_t *depthbuffer;

	context_states_t *states;

} context_t;

context_t* context_create(void);
void set_current_context( context_t* context ); 
context_t* get_current_context(void); 
void context_flush(void);

#endif

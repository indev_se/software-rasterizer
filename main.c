#include "system.h"
#include "inras.h"
#include "vertex.h"
#include "math.h"
#include "vertexbuffer.h"
#include "vec4.h"
#include "mat4x4.h"
#include "renderjob.h"
#include "renderqueue.h"
#include "context.h"
#include "draw.h"
#include "vec2.h"
#include "defaultshaders.h"
#include "shaderlang.h"
#include "triangle.h"

#include "SDL2/SDL.h"

renderjob_t *renderjob = NULL;

void create_color_triangle_vb()
{
	uint8_t *vertices;
	vertex_description_t *description = NULL;
	context_t *context = get_current_context();
	
	vertex_create_description( &description, 0 );
	
	vertex_add_attribute( description, 0, INRAS_FLOAT, 4 ); // xyzw
	vertex_add_attribute( description, 1, INRAS_FLOAT, 3 ); // rgb
	
	vertex_lock_description(description);
	
	vertexbuffer_allocate( &vertices, 3, description );
	
	uint32_t offset = 0;
	vb_write4_float32(vertices, offset, 0.0f, -140.f, 0.f, 1.0f );
	vb_write3_float32(vertices, offset + (4*sizeof(float)), 1.0f, 0.0f, 0.0f );
	
	offset = description->vertex_size;
	vb_write4_float32(vertices, offset, -140.0f, 40.f, 0.f, 1.0f );
	vb_write3_float32(vertices, offset + (4*sizeof(float)), 0.0f, 1.0f, 0.0f );
	
	offset = description->vertex_size * 2.0f;
	vb_write4_float32(vertices, offset, 140.0f, 40.f, 0.f, 1.0f );
	vb_write3_float32(vertices, offset + (4*sizeof(float)), 0.0f, 0.0f, 1.0f );
	
	renderjob_create( &renderjob );
	
	create_vertex_color_shader( &renderjob->program );

	// camera
	mat4x4_translate(context->mat_modelview, 0.0f, 0.0f, -200.0f );

	mat4x4_dup(renderjob->mat_projection, context->mat_projection);
	mat4x4_dup(renderjob->mat_modelview, context->mat_modelview);

	// model
//	mat4x4_rotate_Y(renderjob->mat_modelview, renderjob->mat_modelview, DTOR(12) );

	renderjob->num_vertices = 3;
	renderjob->vertices = vertices;
	renderjob->vertex_description = description;
	renderjob->type = INRAS_TRIANGLE;
	
	renderqueue_add(renderjob);
}

void create_white_triangle_clip_vb()
{
	uint8_t *vertices;
	vertex_description_t *description = NULL;
	context_t *context = get_current_context();

	vertex_create_description( &description, 0 );

	vertex_add_attribute( description, 0, INRAS_FLOAT, 4 ); // xyzw

	vertex_lock_description(description);

	vertexbuffer_allocate( &vertices, 3, description );

	uint32_t offset = 0;
	vb_write4_float32(vertices, offset, 0.0f, -40.f, 0.f, 1.0f );

	offset = description->vertex_size;
	vb_write4_float32(vertices, offset, -140.0f, 40.f, 0.f, 1.0f );

	offset = description->vertex_size * 2.0f;
	vb_write4_float32(vertices, offset, 140.0f, 40.f, 0.f, 1.0f );

	renderjob_create( &renderjob );

	create_vertex_white_shader( &renderjob->program );
      
	// camera
	mat4x4_translate(context->mat_modelview, 0.0f, 0.0f, -200.0f );

	mat4x4_dup(renderjob->mat_projection, context->mat_projection);
	mat4x4_dup(renderjob->mat_modelview, context->mat_modelview);

	// model
//	mat4x4_rotate_Y(renderjob->mat_modelview, renderjob->mat_modelview, DTOR(12) );

	renderjob->num_vertices = 3;
	renderjob->vertices = vertices;
	renderjob->vertex_description = description;
	renderjob->type = INRAS_TRIANGLE;

	renderqueue_add(renderjob);
}

SDL_Window *window = NULL;
SDL_Renderer *renderer = NULL;
SDL_Texture *colorTexture = NULL;
SDL_Texture *depthTexture = NULL;
SDL_Event e;

void sdl_setup()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) == -1) {
		errno_exit(SDL_GetError());
		return;
	}

	window = SDL_CreateWindow("inras", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, SDL_WINDOW_SHOWN);

	if (NULL == window) {
		errno_exit(SDL_GetError());
		return;
	}

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	if (NULL == renderer){
		errno_exit(SDL_GetError());
		return;
	}

	colorTexture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGB24, SDL_TEXTUREACCESS_STREAMING, 800, 600);

	if (NULL == colorTexture){
		errno_exit(SDL_GetError());
		return;
	}

	depthTexture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGB565, SDL_TEXTUREACCESS_STREAMING, 800, 600);

	if (NULL == depthTexture){
		errno_exit(SDL_GetError());
		return;
	}
}

void sdl_flush_color()
{
        context_t *context = get_current_context();

	SDL_UpdateTexture(colorTexture, NULL, context->framebuffer->data, 800 * 3 * sizeof(uint8_t));

	SDL_Rect pos;
	pos.x = 0;
	pos.y = 0;
	SDL_QueryTexture(colorTexture, NULL, NULL, &pos.w, &pos.h);

	SDL_RenderCopy(renderer, colorTexture, NULL, &pos);

	SDL_RenderPresent(renderer);
}

void sdl_flush_depth()
{
	context_t *context = get_current_context();

	SDL_UpdateTexture(depthTexture, NULL, context->depthbuffer->data, 800 * 2 * sizeof(uint8_t));

	SDL_Rect pos;
	pos.x = 0;
	pos.y = 0;
	SDL_QueryTexture(depthTexture, NULL, NULL, &pos.w, &pos.h);

	SDL_RenderCopy(renderer, depthTexture, NULL, &pos);

	SDL_RenderPresent(renderer);
}

uint8_t sdl_events()
{
	while (SDL_PollEvent(&e)){
		if (e.type == SDL_QUIT)
			return 1;
		if (e.type == SDL_KEYDOWN) {
			switch (e.key.keysym.sym) {
			case SDLK_ESCAPE:
				return 1;
			default:
				break;
			}
		}

	}

	return 0;
}

int main(int argc, char** argv)
{
	context_t *context;
//	vec2 from, to;

	inras_init();
	
	create_color_triangle_vb();
	//create_white_triangle_clip_vb();
	
	sdl_setup();

	uint32_t rotY = 0;

	vec3 v0, v1, v2, normal;

	v0[0] = 0; v0[1] = -40; v0[2] = 0;
	v1[0] = -40; v1[1] = 40; v1[2] = 0;
	v2[0] = 40; v2[1] = 40; v2[2] = 0;

	triangle_calculate_normal_cw(normal, v0, v1, v2);
	triangle_calculate_normal_ccw(normal, v0, v1, v2);

	while( sdl_events() == 0 ) {
		context = get_current_context();

		inras_clear_framebuffers();

//		from[0] = 10; from[1] = 10;
//		to[0] = 100; to[1] = 10;

//		draw_line( context->framebuffer, 0xaabbccdd, from, to );

		// rotate
		mat4x4_dup(renderjob->mat_modelview, context->mat_modelview);
		mat4x4_rotate_Y(renderjob->mat_modelview, renderjob->mat_modelview, DTOR(rotY) );
                
                // rotate 180
		//mat4x4_dup(renderjob->mat_modelview, context->mat_modelview);
		//mat4x4_rotate_Y(renderjob->mat_modelview, renderjob->mat_modelview, DTOR(180) );

		inras_flush();
		/*

		mat4x4_dup(renderjob->mat_modelview, context->mat_modelview);
		mat4x4_rotate_Y(renderjob->mat_modelview, renderjob->mat_modelview, DTOR(90+rotY) );

		inras_flush();
*/
		++rotY;
		if (rotY > 360) rotY = 0;

		sdl_flush_color();
	}

	return 0;
}

#ifndef RENDER_QUEUE_H
#define RENDER_QUEUE_H

#include "renderjob.h"

void renderqueue_destroy(void);
void renderqueue_add( renderjob_t *renderjob  );

void renderqueue_process(void);

#endif
#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include "defines.h"

typedef struct {
	uint32_t format;

	uint32_t width;
	uint32_t height;
	
	uint32_t size;

	uint8_t *data;
} framebuffer_t;

framebuffer_t *framebuffer_create( uint32_t width, uint32_t height, uint32_t format );
void framebuffer_clear( framebuffer_t *framebuffer, uint32_t value );

void framebuffer_putpixel24int( framebuffer_t *framebuffer, uint32_t color, uint32_t x, uint32_t y );
void framebuffer_putpixel16float( framebuffer_t *framebuffer, float color, uint32_t x, uint32_t y );
float framebuffer_getpixel16float( framebuffer_t *framebuffer, uint32_t x, uint32_t y );


#endif

#include <string.h>
#include <stdlib.h>

#include "vertex.h"

void vertex_create_description( vertex_description_t **out, uint32_t vertex_format ) 
{
	vertex_description_t *vd = (vertex_description_t*)malloc( sizeof(vertex_description_t) );
	vd->num_attributes = 0;
	vd->vertex_format = vertex_format;
	vd->attributes = NULL;
	
	*out = vd;
}

void vertex_add_attribute( vertex_description_t *description, uint8_t location, uint32_t type, uint32_t num_elements )
{
	vertex_attribute_t *attrib;
	uint8_t type_size = datatype_to_size(type);

	++description->num_attributes;
	if ( description->attributes == NULL )
		description->attributes = (vertex_attribute_t*)calloc(sizeof(vertex_attribute_t), 1);
	else
		description->attributes = (vertex_attribute_t*)realloc(description->attributes, sizeof(vertex_attribute_t) * description->num_attributes );

	attrib = &description->attributes[description->num_attributes-1];
	attrib->type = type;
	attrib->offset = 0;
	attrib->location = location;
	attrib->num_elements = num_elements;
	attrib->size = type_size * num_elements;
}

void vertex_lock_description( vertex_description_t *description )
{
	uint32_t i, vertex_size;
	vertex_attribute_t *attr;
	
	vertex_size = 0;
	
	for ( i = 0; i < description->num_attributes; ++i )
	{
		attr = &description->attributes[i];
		attr->offset = vertex_size;
		
		vertex_size += attr->size;
	}
	
	description->vertex_size = vertex_size;
}

void vertex_attribute_get_by_location( vertex_attribute_t **out, vertex_description_t *description, uint32_t location )
{
	uint32_t i;
	
	for ( i = 0; i < description->num_attributes; ++i )
	{
		if ( description->attributes[i].location == location )
		{
			*out = &description->attributes[i];
			return;
		}
	}
	
	*out = NULL;
}
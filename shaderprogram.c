#include "shaderprogram.h"

#include <stdlib.h>

void shader_destroy_properties(shader_properties_t *properties) {
    if (NULL == properties)
        return;

    for (uint8_t i = 0; i < properties->length; ++i) {
        free(properties->properties[i].values);
    }
    free(properties->properties);

    free(properties);
}

void shader_ps_create(pixel_shader_t **ps) {
    pixel_shader_t *_ps = malloc(sizeof (pixel_shader_t));

    _ps->func = NULL;
    _ps->in = malloc(sizeof (ps_in_t));
    _ps->out = malloc(sizeof (ps_out_t));

    _ps->in->uniforms = malloc(sizeof (shader_properties_t));
    _ps->in->varying = malloc(sizeof (shader_properties_t));

    _ps->in->uniforms->properties = NULL;
    _ps->in->varying->properties = NULL;

    _ps->in->uniforms->length = 0;
    _ps->in->varying->length = 0;

    *ps = _ps;
}

void shader_ps_destroy(pixel_shader_t *ps) {
    shader_destroy_properties(ps->in->uniforms);
    shader_destroy_properties(ps->in->varying);

    free(ps->out->result);

    free(ps->in);
    free(ps->out);

    free(ps);
}

void shader_vs_create(vertex_shader_t **vs) {
    vertex_shader_t *_vs = malloc(sizeof (vertex_shader_t));

    _vs->func = NULL;
    _vs->in = malloc(sizeof (vs_in_t));
    _vs->out = malloc(sizeof (vs_out_t));

    _vs->in->attributes = malloc(sizeof (shader_properties_t));
    _vs->in->uniforms = malloc(sizeof (shader_properties_t));
    _vs->in->varying = malloc(sizeof (shader_properties_t));

    _vs->out->varying = malloc(sizeof (shader_properties_t));

    _vs->in->attributes->properties = NULL;
    _vs->in->uniforms->properties = NULL;
    _vs->in->varying->properties = NULL;

    _vs->in->attributes->length = 0;
    _vs->in->uniforms->length = 0;
    _vs->in->varying->length = 0;

    _vs->out->varying->properties = NULL;

    _vs->out->varying->length = 0;

    *vs = _vs;
}

void shader_vs_destroy(vertex_shader_t *vs) {
    shader_destroy_properties(vs->in->attributes);
    shader_destroy_properties(vs->in->uniforms);
    shader_destroy_properties(vs->in->varying);

    shader_destroy_properties(vs->out->varying);
    free(vs->out->result);

    free(vs->in);
    free(vs->out);

    free(vs);
}

uint8_t shader_add_property(shader_properties_t *properties, uint8_t property_length) {
    ++properties->length;
    if (NULL == properties->properties)
        properties->properties = calloc(sizeof (shader_property_t), 1);
    else
        properties->properties = realloc(properties->properties, sizeof (shader_property_t) * properties->length);

    shader_property_t *property = &properties->properties[properties->length - 1];
    property->values = malloc(sizeof (float)*property_length);
    property->length = property_length;

    return properties->length - 1; // return the location of the property
}

void shader_program_compile(vertex_shader_t *vs, pixel_shader_t *ps, shader_program_t **program) {
    // some validation
    if (NULL == ps->in->varying || NULL == vs->out->varying) {
        *program = NULL;
        return;
    }

    if (ps->in->varying->length != vs->out->varying->length) {
        *program = NULL;
        return;
    }

    if (vs->in->attributes->length == 0) {
        *program = NULL;
        return;
    }

    shader_program_t *_program = malloc(sizeof (shader_program_t));
    _program->pixel_shader = ps;
    _program->vertex_shader = vs;

    *program = _program;
}

void shader_vs_alloc_out(vertex_shader_t *vs, vs_out_t **_out) {
    vs_out_t *out;
    uint32_t i;

    out = malloc(sizeof (vs_out_t));
    out->varying = malloc(sizeof (shader_properties_t));

    out->varying->properties = NULL;
    out->varying->length = 0;

    for (i = 0; i < vs->out->varying->length; ++i)
        shader_add_property(out->varying, vs->out->varying->properties[i].length);

    *_out = out;
}

void shader_vs_alloc_out_varying(vertex_shader_t *vs, vs_out_t *out) {
    uint32_t i;

    out->varying = malloc(sizeof (shader_properties_t));

    out->varying->properties = NULL;
    out->varying->length = 0;

    for (i = 0; i < vs->out->varying->length; ++i)
        shader_add_property(out->varying, vs->out->varying->properties[i].length);
}

void shader_vs_out_copy(vs_out_t *dst, vs_out_t *src) {
    uint32_t i;
    vec4_dup(dst->result, src->result);

    for (i = 0; i < src->varying->length; ++i) {
        vec_n_dup(src->varying->properties[i].length, dst->varying->properties[i].values, src->varying->properties[i].values);
    }
}

void shader_vs_free_out(vs_out_t *out) {
    shader_destroy_properties(out->varying);
    free(out->result);
    free(out);
}

void shader_vs_free_out_varying(vs_out_t *out) {
    shader_destroy_properties(out->varying);
}

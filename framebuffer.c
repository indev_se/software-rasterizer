#include <stdlib.h>
#include <string.h>

#include "defines.h"
#include "types.h"
#include "framebuffer.h"

framebuffer_t *framebuffer_create( uint32_t width, uint32_t height, uint32_t format )
{
	uint8_t type_size = format_to_size(format);

	framebuffer_t *framebuffer = (framebuffer_t*)malloc( sizeof(framebuffer_t) );

	framebuffer->format = format;
	framebuffer->width = width;
	framebuffer->height = height;
	framebuffer->size = width * height * type_size;
	
	framebuffer->data = (uint8_t*)malloc( framebuffer->size );

	return framebuffer;
}

void framebuffer_clear( framebuffer_t *framebuffer, uint32_t value )
{
	memset( framebuffer->data, value, framebuffer->size );
}

void framebuffer_putpixel24int( framebuffer_t *framebuffer, uint32_t color, uint32_t x, uint32_t y )
{
	uint32_t row = (y*3*framebuffer->width);

	// check bounds
	if ( x > framebuffer->width || y > framebuffer->height )
		return;

	framebuffer->data[ row + (x*3) ] = color & 0xff;
	framebuffer->data[ row + (x*3) + 1 ] = (color >> 8) & 0xff;
	framebuffer->data[ row + (x*3) + 2 ] = (color >> 16) & 0xff;
}

void framebuffer_putpixel16float( framebuffer_t *framebuffer, float color, uint32_t x, uint32_t y )
{
	uint32_t row = (y*2*framebuffer->width);

	// check bounds
	if ( x > framebuffer->width || y > framebuffer->height )
		return;

	uint16_t value = (uint16_t)(color * 0xffff);
	framebuffer->data[ row + (x*2) ] = value & 0xff;
	framebuffer->data[ row + (x*2) + 1 ] = (value >> 8) & 0xff;
}

float framebuffer_getpixel16float( framebuffer_t *framebuffer, uint32_t x, uint32_t y )
{
	uint32_t row = (y*2*framebuffer->width);

	// check bounds
	if ( x > framebuffer->width || y > framebuffer->height )
		return 0.0f;

	uint8_t v1 = framebuffer->data[ row + (x*2) ];
	uint8_t v2 = framebuffer->data[ row + (x*2) + 1 ];
	uint16_t value = v1 | v2 << 8;

	return (float)((float)value / (float)0xffff);
}

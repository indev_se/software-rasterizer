#ifndef VEC3_H
#define VEC3_H

#include <math.h>
#include <string.h>

#include "defines.h"

typedef float vec3[3];
STATIC_INLINE void vec3_dup(vec3 r, vec3 a)
{
	int i;
	for(i=0; i<3; ++i)
		r[i] = a[i];
}
STATIC_INLINE void vec3_add(vec3 r, vec3 a, vec3 b)
{
	int i;
	for(i=0; i<3; ++i)
		r[i] = a[i] + b[i];
}
STATIC_INLINE void vec3_sub(vec3 r, vec3 a, vec3 b)
{
	int i;
	for(i=0; i<3; ++i)
		r[i] = a[i] - b[i];
}
STATIC_INLINE void vec3_scale(vec3 r, vec3 v, float s)
{
	int i;
	for(i=0; i<3; ++i)
		r[i] = v[i] * s;
}
STATIC_INLINE float vec3_mul_inner(vec3 a, vec3 b)
{
	float p = 0.;
	int i;
	for(i=0; i<3; ++i)
		p += b[i]*a[i];
	return p;
}
STATIC_INLINE void vec3_mul_cross(vec3 r, vec3 a, vec3 b)
{
	vec3 c;
	c[0] = a[1]*b[2] - a[2]*b[1];
	c[1] = a[2]*b[0] - a[0]*b[2];
	c[2] = a[0]*b[1] - a[1]*b[0];
	memcpy(r, c, sizeof(c));
}
STATIC_INLINE float vec3_len(vec3 v)
{
	return sqrtf(vec3_mul_inner(v,v));
}
STATIC_INLINE void vec3_norm(vec3 r, vec3 v)
{
	float k = 1.0f / vec3_len(v);
	vec3_scale(r, v, k);
}
STATIC_INLINE float vec3_dot(vec3 a, vec3 b)
{
	float dot = (a[0] * b[0]) + (a[1] * b[1]) + (a[2] * b[2]);
	return dot;
}
STATIC_INLINE uint8_t vec3_eql(vec3 a, vec3 b)
{
	return ( (a[0] - b[0]) + (a[1] - b[1]) + (a[2] - b[2]) ) == 0;
}
#endif

#ifndef SHADERSTRUCTS_H_
#define SHADERSTRUCTS_H_

#include "defines.h"
#include "vec4.h"

typedef struct {

	float *values;
	uint8_t length;

} shader_property_t;


typedef struct {

	shader_property_t *properties;
	uint8_t length;

} shader_properties_t;

typedef struct {

	shader_properties_t *varying;
	vec4 result;

} vs_out_t;

typedef struct {

	shader_properties_t *uniforms;
	shader_properties_t *varying;
	shader_properties_t *attributes;

} vs_in_t;

typedef struct {

	vec4 result;

} ps_out_t;

typedef struct {

	shader_properties_t *uniforms;
	shader_properties_t *varying;

} ps_in_t;

typedef void (*vertex_shader_func_ptr) ( const vs_in_t*, vs_out_t* );
typedef void (*pixel_shader_func_ptr) ( const ps_in_t*, ps_out_t* );

typedef struct {

	vs_out_t *out;
	vs_in_t *in;

	vertex_shader_func_ptr func;

} vertex_shader_t;

typedef struct {

	ps_out_t *out;
	ps_in_t *in;

	pixel_shader_func_ptr func;

} pixel_shader_t;

typedef struct {

	vertex_shader_t *vertex_shader;
	pixel_shader_t *pixel_shader;

} shader_program_t;

#endif /* SHADERSTRUCTS_H_ */

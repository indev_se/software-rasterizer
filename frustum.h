#ifndef FRUSTUM_H
#define FRUSTUM_H

#include "shaderstructs.h"
#include "plane.h"
#include "mat4x4.h"

/**
* calculate 6 planes to make up the frustum volume from a 
* projection matrix, can be multipled with the modelview matrix
**/
void frustum_from_mat( mat4x4 mat, plane frustum_planes[6] );

void frustum_clip_triangle( plane frustum_planes[6], vs_out_t in_vertices[3], vs_out_t out_vertices[12], vs_out_t tmp_vertices[12], uint32_t *num_triangles );

#endif

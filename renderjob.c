#include "renderjob.h"
#include "defines.h"
#include "mat4x4.h"
#include "vertexbuffer.h"
#include "context.h"
#include "frustum.h"
#include "vertextransform.h"
#include "rasterizer.h"
#include "shaderlang.h"

#include <stdio.h>
#include <stdlib.h>

void renderjob_rasterize_triangle_line(renderjob_t *renderjob);
void renderjob_rasterize_triangle(renderjob_t *renderjob);
void renderjob_render_triangles(renderjob_t *renderjob);

void renderjob_create(renderjob_t **out) {
    renderjob_t* renderjob = malloc(sizeof (renderjob_t));

    mat4x4_identity(renderjob->mat_modelview);
    mat4x4_identity(renderjob->mat_projection);

    renderjob->vertex_description = NULL;

    renderjob->num_vertices = 0;
    renderjob->vertices = NULL;

    renderjob->states = malloc(sizeof (renderjob_states_t));
    renderjob->states->face_culling = INRAS_CULL_FRONT;

    *out = renderjob;
}

void renderjob_process(renderjob_t *renderjob) {
    // transform all xyzw values of the vertices
    mat4x4 mat_mvp;

    mat4x4_mul(mat_mvp, renderjob->mat_projection, renderjob->mat_modelview);

    // calculate the frustum
    frustum_from_mat(mat_mvp, renderjob->frustum_planes);
    /*plane_normalize(renderjob->frustum_planes[0]);
    plane_normalize(renderjob->frustum_planes[1]);
    plane_normalize(renderjob->frustum_planes[2]);
    plane_normalize(renderjob->frustum_planes[3]);
    plane_normalize(renderjob->frustum_planes[4]);
    plane_normalize(renderjob->frustum_planes[5]);*/

    // bind the global shader values
    shlang_register_global_mat4x4(SHADER_MVP_MATRIX, mat_mvp);
    shlang_register_global_mat4x4(SHADER_MODELVIEW_MATRIX, renderjob->mat_modelview);
    shlang_register_global_mat4x4(SHADER_PROJECTION_MATRIX, renderjob->mat_projection);

    // rasterize the transformed vertices
    renderjob_render_triangles(renderjob);
}

void renderjob_render_triangles(renderjob_t *renderjob) {
    uint32_t i, vert, tri;
    vertex_shader_t *vs;
    uint32_t num_vertices = renderjob->num_vertices;

    vs = renderjob->program->vertex_shader;
    vs_out_t *v0_out, *v1_out, *v2_out;
    vs_out_t *v0, *v1, *v2;

    // allocate memory for the out vales
    shader_vs_alloc_out(vs, &v0_out);
    shader_vs_alloc_out(vs, &v1_out);
    shader_vs_alloc_out(vs, &v2_out);

    for (vert = 0; vert < num_vertices; vert += 3) {
        // read the attributes and send them to the vertex shader
        uint8_t *v_ptr;
        uint32_t property_size;

        // v0
        v_ptr = renderjob->vertices + (vert * renderjob->vertex_description->vertex_size);
        for (i = 0; i < vs->in->attributes->length; ++i) {
            property_size = sizeof (float) * vs->in->attributes->properties[i].length;
            memcpy(vs->in->attributes->properties[i].values, v_ptr, property_size);
            v_ptr += property_size;
        }
        vs->func(vs->in, v0_out);

        // v1
        v_ptr = renderjob->vertices + ((vert + 1) * renderjob->vertex_description->vertex_size);
        for (i = 0; i < vs->in->attributes->length; ++i) {
            property_size = sizeof (float) * vs->in->attributes->properties[i].length;
            memcpy(vs->in->attributes->properties[i].values, v_ptr, property_size);
            v_ptr += property_size;
        }
        vs->func(vs->in, v1_out);

        // v2
        v_ptr = renderjob->vertices + ((vert + 2) * renderjob->vertex_description->vertex_size);
        for (i = 0; i < vs->in->attributes->length; ++i) {
            property_size = sizeof (float) * vs->in->attributes->properties[i].length;
            memcpy(vs->in->attributes->properties[i].values, v_ptr, property_size);
            v_ptr += property_size;
        }
        vs->func(vs->in, v2_out);

        // culling and clipping here I guess? should move this out from the rasterizer, don't have vertex calcs here
        #define MAX_NUM_VERTICES 18

        vs_out_t in_tri[3], out_tris[MAX_NUM_VERTICES], tmp_tris[MAX_NUM_VERTICES];
        uint32_t num_triangles;

        in_tri[0] = *v0_out;
        in_tri[1] = *v1_out;
        in_tri[2] = *v2_out;

        // allocate room for the out vertices
        for (i = 0; i < MAX_NUM_VERTICES; ++i) {
            shader_vs_alloc_out_varying(vs, &out_tris[i]);
            shader_vs_alloc_out_varying(vs, &tmp_tris[i]);
        }

        frustum_clip_triangle(renderjob->frustum_planes, in_tri, out_tris, tmp_tris, &num_triangles);

        for (tri = 0; tri < num_triangles; ++tri) {
            v0 = &out_tris[(tri * 3)];
            v1 = &out_tris[(tri * 3) + 1];
            v2 = &out_tris[(tri * 3) + 2];

            // convert to homogenous coordinates
            v0->result[0] = v0->result[0] / v0->result[3];
            v0->result[1] = v0->result[1] / v0->result[3];
            v0->result[2] = v0->result[2] / v0->result[3];

            v1->result[0] = v1->result[0] / v1->result[3];
            v1->result[1] = v1->result[1] / v1->result[3];
            v1->result[2] = v1->result[2] / v1->result[3];

            v2->result[0] = v2->result[0] / v2->result[3];
            v2->result[1] = v2->result[1] / v2->result[3];
            v2->result[2] = v2->result[2] / v2->result[3];

            //if (renderjob->type == INRAS_TRIANGLE_LINE)
            //if (renderjob->type == INRAS_TRIANGLE)
                rasterize_triangle(v0, v1, v2, renderjob->program);
                rasterize_triangle_line(v0, v1, v2, renderjob->program);
            
        }

        for (i = 0; i < MAX_NUM_VERTICES; ++i) {
           //shader_vs_free_out_varying( &out_tris[i] );
           shader_vs_free_out_varying( &tmp_tris[i] );
        }

    }

    // free
    shader_vs_free_out(v0_out);
    shader_vs_free_out(v1_out);
    shader_vs_free_out(v2_out);
}
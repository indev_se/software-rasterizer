#ifndef DEFINES_H
#define DEFINES_H

#include <stdint.h>

#ifdef _MSC_VER
#define STATIC_INLINE static __inline
#else
#define STATIC_INLINE static inline
#endif

/* DataType */
#define INRAS_BYTE                          0x0100
#define INRAS_UNSIGNED_BYTE                 0x0101
#define INRAS_SHORT                         0x0102
#define INRAS_UNSIGNED_SHORT                0x0103
#define INRAS_INT                           0x0104
#define INRAS_UNSIGNED_INT                  0x0105
#define INRAS_FLOAT                         0x0106
#define INRAS_2_BYTES                       0x0107
#define INRAS_3_BYTES                       0x0108
#define INRAS_4_BYTES                       0x0109
#define INRAS_DOUBLE                        0x010A

/* Pixel formats */
#define INRAS_RGB24							0x0200
#define INRAS_RGBA32						0x0201
#define INRAS_DEPTH16						0x0202

/* Primitives */
#define INRAS_TRIANGLE						0x0300
#define INRAS_TRIANGLE_LINE					0x0301

/** Settings consts and other **/
#define INRAS_FRONTFACE_CW					0x0400
#define INRAS_FRONTFACE_CCW					0x0401

#define INRAS_CULL_FRONT					0x0402
#define INRAS_CULL_BACK						0x0403
#define INRAS_CULL_FRONT_AND_BACK			0x0405
#define INRAS_CULL_NONE						0x0405

/* Shader lang constants */
#define INRAS_SHADERLANG					0x0500

/* Vertex formats bits */
#define INRAS_VF_XYZ						1
#define INRAS_VF_NORMAL						1 << 1
#define INRAS_VF_COLOR_RGB					1 << 2

#define INRAS_VF_TEX0						1 << 5

#endif

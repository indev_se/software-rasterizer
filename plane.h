#ifndef PLANE_H
#define PLANE_H

#include "defines.h"
#include "vec3.h"
#include "vec4.h"

#define PLANE_DENOM		0.001f

#define PLANE_LINE_UNKNOWN		0x0
#define PLANE_LINE_OUTSIDE		0x1
#define PLANE_LINE_INSIDE		0x2
#define PLANE_LINE_IN_OUT		0x4
#define PLANE_LINE_OUT_IN		0x8

#define PLANE_POINTS_INSIDE		16
#define PLANE_POINTS_OUTSIDE	32
#define PLANE_POINTS_BOTH		64
#define PLANE_POINTS_ONPLANE	128

typedef float plane[4];


STATIC_INLINE void plane_normalize(plane result)
{
	float length;

	length = vec3_len(result);

	result[0] /= length;
	result[1] /= length;
	result[2] /= length;
	result[3] /= length;
}

STATIC_INLINE void plane_set_coefficients(float a, float b, float c, float d, plane result)
{
	result[0] = a;
	result[1] = b;
	result[2] = c;
	result[3] = d;

	vec3_norm(result, result);
}


STATIC_INLINE float plane_distance_to_point( plane p, vec4 point)
{
	float dot = vec3_dot(p, point);

	return dot + p[3];
}

STATIC_INLINE uint32_t plane_classify_points( plane p, vec4 *points, uint32_t num_points )
{
	uint32_t num_pos, num_neg;
	float distance;

	num_pos = num_neg = 0;

	for ( uint32_t i = 0; i < num_points; ++i )
	{
		distance = plane_distance_to_point(p, points[i]);

		if ( distance > PLANE_DENOM ) num_pos++;
		else if ( distance < -PLANE_DENOM ) num_neg++;
	}

	if( num_pos > 0 && num_neg == 0 )
		return PLANE_POINTS_INSIDE;
	else if( num_pos == 0 && num_neg > 0 )
		return PLANE_POINTS_OUTSIDE;
	else if( num_pos > 0 && num_neg > 0 )
		return PLANE_POINTS_BOTH;
	else
		return PLANE_POINTS_ONPLANE;

}

STATIC_INLINE uint8_t plane_classify_line( plane p, float distance_p0, float distance_p1 )
{
	if( distance_p0 < 0 && distance_p1 < 0 )
		return PLANE_LINE_OUTSIDE;
	else if( distance_p0 > 0 && distance_p1 > 0 )
		return PLANE_LINE_INSIDE;
	else if( distance_p0 > 0 && distance_p1 < 0 )
		return PLANE_LINE_IN_OUT;
	else
		return PLANE_LINE_OUT_IN;
}


STATIC_INLINE void plane_clip_line( plane clip, vec4 p0, vec4 p1, vec4 out[2], uint32_t *num_points )
{
	float distance_p0, distance_p1;
	float factor;

	distance_p0 = plane_distance_to_point(clip, p0);
	distance_p1 = plane_distance_to_point(clip, p1);

	factor = distance_p0 / (distance_p0-distance_p1);

	uint8_t line_pos = plane_classify_line(clip, distance_p0, distance_p1);

	*num_points = 0;
	if ( line_pos == PLANE_LINE_INSIDE )
	{
		vec4_dup(out[0], p1);
		*num_points = 1;
	}
	else if ( line_pos == PLANE_LINE_IN_OUT )
	{
		out[0][0] = p0[0] + (p1[0] - p0[0]) * factor;
		out[0][1] = p0[1] + (p1[1] - p0[1]) * factor;
		out[0][2] = p0[2] + (p1[2] - p0[2]) * factor;
		out[0][3] = p0[3] + (p1[3] - p0[3]) * factor;
		*num_points = 1;
	}
	else if ( line_pos == PLANE_LINE_OUT_IN )
	{
		out[0][0] = p0[0] + (p1[0] - p0[0]) * factor;
		out[0][1] = p0[1] + (p1[1] - p0[1]) * factor;
		out[0][2] = p0[2] + (p1[2] - p0[2]) * factor;
		out[0][3] = p0[3] + (p1[3] - p0[3]) * factor;
		vec4_dup(out[1], p1);
		*num_points = 2;
	}
}

STATIC_INLINE void plane_clip_points( plane clip, vec4 *in_points, vec4 *out_points, uint32_t num_in_points, uint32_t *num_out_points )
{
	uint32_t i, j, k, num_clipped_points;
	vec4 p0, p1, p_clipped[2];

	*num_out_points = 0;
        
	for( i = 0 ; i < num_in_points; ++i )
	{
		j = (i+1) % num_in_points;

                vec4_dup(p0, in_points[i]);
		vec4_dup(p1, in_points[j]);

		plane_clip_line(clip, p0, p1, p_clipped, &num_clipped_points);

		for ( k = 0; k < num_clipped_points; ++k ) {
			vec4_dup(out_points[*num_out_points], p_clipped[k]);
			(*num_out_points)++;
		}
	}

}

STATIC_INLINE void plane_from_points(vec3 v0, vec3 v1, vec3 v2, plane result)
{
	float length;

	// calculate the normal of the plane
	float dx1 = v1[0] - v0[0];
	float dy1 = v1[1] - v0[1];
	float dz1 = v1[2] - v0[2];

	float dx2 = v2[0] - v0[0];
	float dy2 = v2[1] - v0[1];
	float dz2 = v2[2] - v0[2];

	result[0] = dy1 * dz2 - dy2 * dz1;
	result[1] = dz1 * dx2 - dz2 * dx1;
	result[2] = dx1 * dy2 - dx2 * dy1;

	// normalize the normal
	length = vec3_len(result);

	result[0] /= length;
	result[1] /= length;
	result[2] /= length;
	
	// calculate the denominator
	result[3] = result[0] * v1[0] + result[1] * v1[1] + result[2] * v1[2];
}

#endif

#include "defines.h"
#include "types.h"

uint8_t datatype_to_size( uint32_t type )
{
	switch ( type )
	{
		case INRAS_BYTE:
		case INRAS_UNSIGNED_BYTE:
			return 1;

		case INRAS_SHORT:
		case INRAS_UNSIGNED_SHORT:
		case INRAS_2_BYTES:
			return 2;

		case INRAS_INT:
		case INRAS_UNSIGNED_INT:
		case INRAS_4_BYTES:
		case INRAS_FLOAT:
		case INRAS_DOUBLE:
			return 4;
	
		case INRAS_3_BYTES:
			return 3;
	}

	return 0;
}

uint8_t format_to_size( uint32_t type )
{
	switch ( type )
	{
		case INRAS_DEPTH16:
			return 2;

		case INRAS_RGB24:
			return 3;

		case INRAS_RGBA32:
			return 4;
	}

	return 0;
}

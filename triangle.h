/*
 * triangle.h
 *
 *  Created on: 16 nov 2013
 *      Author: Kristian
 */

#ifndef TRIANGLE_H_
#define TRIANGLE_H_

void triangle_calculate_normal_cw( vec3 norm, vec3 v0, vec3 v1, vec3 v2 );
void triangle_calculate_normal_ccw( vec3 norm, vec3 v0, vec3 v1, vec3 v2 );
void triangle_clip_plane( plane clip_plane, vs_out_t in_vertices[3], vs_out_t *out_vertices, uint32_t *num_triangles );
uint8_t triangle_is_valid( vec3 v0, vec3 v1, vec3 v2 );

#endif /* TRIANGLE_H_ */

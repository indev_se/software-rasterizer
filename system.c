#include "system.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void errno_exit(const char *err)
{
  fprintf(stderr, "%s error %d, %s\n", err, errno, strerror(errno));
  exit(EXIT_FAILURE);
}

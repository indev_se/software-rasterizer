#ifndef VERTEXTRANSFORM_H_
#define VERTEXTRANSFORM_H_

#include "defines.h"
#include "vertex.h"
#include "vec4.h"
#include "mat4x4.h"

void transform_vec4( vertex_attribute_t *attribute, uint32_t vertex_size, uint8_t *vertices, uint32_t num_vertices, mat4x4 matrix );

#endif /*VERTEXTRANSFORM_H_*/

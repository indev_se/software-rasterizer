#include "inras.h"
#include "framebuffer.h"
#include "context.h"
#include "renderqueue.h"

void inras_init(void)
{
	set_current_context( context_create() );
	
}

void inras_clear_framebuffers(void)
{
	context_t *context = get_current_context();
	
	framebuffer_clear( context->framebuffer, 0x0 );
	framebuffer_clear( context->depthbuffer, 0xff );
}

void inras_flush(void)
{
	// process all renderjobs in the queue
	renderqueue_process();
}

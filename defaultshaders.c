#include "defaultshaders.h"
#include "shaderstructs.h"
#include "shaderprogram.h"
#include "shaderlang.h"

#include "vec4.h"
#include "vec3.h"
#include "mat4x4.h"

void create_vertex_color_shader_vs_impl( const vs_in_t *in, vs_out_t *out )
{
	vec4 pos;
	vec3 color;
	mat4x4 mvp;

	shlang_get_global_mat4x4(SHADER_MVP_MATRIX, mvp);

	mat4x4_mul_vec4( pos, mvp, in->attributes->properties[0].values );
	vec3_dup(color, in->attributes->properties[1].values);

	// copy the color to the pixel shader
	vec3_dup(out->varying->properties[0].values, color);

	// transform the position to the result
	vec4_dup(out->result, pos);
}

void create_vertex_color_shader_ps_impl( const ps_in_t *in, ps_out_t *out )
{
	// simply copy the color
	vec3 rgb;
	vec3_dup(rgb, in->varying->properties[0].values);

	out->result[0] = rgb[0];
	out->result[1] = rgb[1];
	out->result[2] = rgb[2];
	out->result[3] = 1;
}

void create_vertex_color_shader( shader_program_t **program )
{
	vertex_shader_t *vs = NULL;
	pixel_shader_t *ps = NULL;

	shader_vs_create(&vs);
	shader_ps_create(&ps);

	uint8_t pos_loc = shader_add_property(vs->in->attributes, 4); // xyzw
	uint8_t col_loc = shader_add_property(vs->in->attributes, 3); // rgb

	shader_add_property(vs->out->varying, 3); // rgb out
	shader_add_property(ps->in->varying, 3); // rgb in to ps

	vs->func = create_vertex_color_shader_vs_impl;
	ps->func = create_vertex_color_shader_ps_impl;

	shader_program_compile( vs, ps, program );
}

void create_vertex_white_shader_vs_impl( const vs_in_t *in, vs_out_t *out )
{
	vec4 pos;
	mat4x4 mvp;

	shlang_get_global_mat4x4(SHADER_MVP_MATRIX, mvp);

	mat4x4_mul_vec4( pos, mvp, in->attributes->properties[0].values );

	// transform the position to the result
	vec4_dup(out->result, pos);
}

void create_vertex_white_shader_ps_impl( const ps_in_t *in, ps_out_t *out )
{
	out->result[0] = 1.0f;
	out->result[1] = 1.0f;
	out->result[2] = 1.0f;
	out->result[3] = 1.0f;
}

void create_vertex_white_shader( shader_program_t **program )
{
	vertex_shader_t *vs = NULL;
	pixel_shader_t *ps = NULL;

	shader_vs_create(&vs);
	shader_ps_create(&ps);

	uint8_t pos_loc = shader_add_property(vs->in->attributes, 4); // xyzw

	vs->func = create_vertex_white_shader_vs_impl;
	ps->func = create_vertex_white_shader_ps_impl;

	shader_program_compile( vs, ps, program );
}

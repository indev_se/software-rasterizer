#include "defines.h"
#include "vertex.h"
#include "vec4.h"
#include "mat4x4.h"

#include "vertexbuffer.h"
#include "vertextransform.h"

void transform_vec4( vertex_attribute_t *attribute, uint32_t vertex_size, uint8_t *vertices, uint32_t num_vertices, mat4x4 matrix )
{
	uint32_t i;
	vec4 v;
	
	for ( i = 0; i < num_vertices; ++i )
	{
		vb_read4_float32( vertices, i * vertex_size, &v[0], &v[1], &v[2], &v[3] );
		
		mat4x4_mul_vec4( v, matrix, v );
		
		vb_write4_float32( vertices, i * vertex_size, v[0], v[1], v[2], v[3] );
	}
}